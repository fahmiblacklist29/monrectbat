const express = require("express");
const bodyParser = require('body-parser');
const session = require("express-session");
const __ = require("lodash");
const cors = require("cors");
const jwt = require("jsonwebtoken");
const { Server } = require("socket.io");
const http = require("http");
const url = require("url");
const fs = require('fs');
const path = require("path");
const { Telegraf } = require("telegraf");

const Router = require("./routes/routes");
const { checkForNotif } = require("./utils/event");
const { secret, appname, botToken } = require("./config/config");
const { insertUserTelegram, deleteUserTelegram, getUserTelegramByChatId } = require("./models/user-telegram");

const app = express();
const server = http.createServer(app);
const io = new http.Server(server);
const port = 3001;

const buffer = require("buffer");
const { insert } = require("postgresql-js");
const bot = new Telegraf(botToken);

app.set('view engine', 'ejs');
app.set('socketio', io);
app.set('jwt', jwt)
app.use(express.static('public'));
app.use(express.json());
app.use(cors());
app.use(Router)

const userToken = new Array()
const kode = new Array()


bot.start((ctx) => {
    // console.log(ctx);
    ctx.replyWithMarkdown("Welcome to *MonRecti BOT*!" +
    "\nUse Command :" + 
    "\n - */register* to subscribe notification" +
    "\n - */unregister* to unsubscribe notification");
});
// bot.help((ctx) => ctx.reply('Send me a sticker'));
bot.command('register', (ctx) => {
    var roomName;
    if (ctx.chat.type == "private") {
        roomName = ctx.chat.username;
    } else {
        roomName = ctx.chat.title;
    }
    var data = {
        "body": {
            "name": roomName,
            "chatId": ctx.chat.id
        }
    }
    getUserTelegramByChatId(data, (err, resUserTele) => {
        if (err == null) {
            if (resUserTele.length > 0) {
                ctx.reply('This chat room has been registered.')
            } else {
                insertUserTelegram(data, (err, results) => {
                    if (err == null) {
                        ctx.reply('Register Success\nI will inform you when something happens.')
                    } else {
                        ctx.reply('Register Failed!\nPlease inform admin.')
                    }
                })
            }
        } else {
            ctx.reply('Register Failed!\nPlease inform admin.')
        }
    })
});

bot.command('unregister', (ctx) => {
    var data = {
        "body": {
            "chatId": ctx.chat.id
        }
    }
    deleteUserTelegram(data, (err, results) => {
        if (err == null) {
            ctx.reply('Unregister Success')
        } else {
            ctx.reply('Unregister Failed!\nPlease inform admin.')
        }
    })
});

bot.launch();

// setInterval()
setInterval(checkForNotif, (10 * 1000));

app.listen(port, () => {
    console.log(`Example app listening at http://localhost:${port}`)
})