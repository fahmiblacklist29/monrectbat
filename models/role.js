const { pool } = require("../config/database.js");


const getRole = (result) => {
    pool.query("SELECT * FROM im_role ORDER BY id_role ASC", (error, results) => {
        if (error) {
            result(error,null);
        }
        result(null, results.rows);
    })
}

const getRoleById = (request, result) => {
    const id = request.params.id;

    pool.query("SELECT * FROM im_role WHERE id_role = $1", [id], (error, results) => {
        if (error) {
            result(error, null);
        }
        result(null, results.rows);
    })
}

module.exports = { getRole, getRoleById };