const { pool } = require("../config/database.js");


const getWilayah = (result) => {
    pool.query("SELECT * FROM mt_wilayah WHERE flag = 'Y' ORDER BY id ASC", (error, results) => {
        if (error) {
            result(error, null);
        }

        result(null, results.rows);
    })
}

const getWilayahById = (request, result) => {
    const id = request.params.id;
    pool.query("SELECT * FROM mt_wilayah WHERE flag = 'Y' AND id = $1", [id], (error, results) => {

        if (error) {
            result(error, null);
        }
        result(null, results.rows);
    })
}

const getWilayahByArea = (request, result) => {
    const id = request.params.id;
    pool.query("SELECT * FROM mt_wilayah WHERE flag = 'Y' AND area_id = $1", [id], (error, results) => {
        if (error) {
            result(error, null);
        }
        result(null, results.rows);
    })
}

const insertWilayah = async (request, result) => {
    const { wilayahName, desaId, desaName, kecamatanId, kecamatanName, kotaId, kotaName, provinsiId, provinsiName, areaId, longitude, latitude } = request.body;
    const byWho = request.cookies.username;
    var seqId;

    // console.log(request.body);

    // (async () => {
    //     // note: we don't try/catch this because if connecting throws an exception
    //     // we don't need to dispose of the client (it will be undefined)
    //     const client = await pool.connect();
    //     try {
    //         await client.query('BEGIN');

    //         const querySeq = "SELECT nextval('seq_wilayah');"
    //         const seqId = await client.query(querySeq);
    //         const custArea = desaId + ("0" + seqId).slice(-3);

    //         console.log("areaid ===> " +custArea)
    //         const valueQuery = [seqId, wilayahName, desaId, desaName, kecamatanIid, kecamatanName, kotaId, kotaName, provinsiId, provinsiName, custArea, longitude, latitude];

    //         const queryInsert = " INSERT INTO mt_wilayah(" +
    //             " id, wilayah_name, desa_id, desa_name, kecamatan_id, kecamatan_name, kota_id, kota_name, provinsi_id, provinsi_name, area_id, longitude, latitude, created_date, last_update, flag, action)" +
    //             " VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, $13, now(), now(), 'Y', 'C');";

    //         await client.query(queryInsert, valueQuery);

    //         await client.query('COMMIT');
    //     } catch (e) {
    //         await client.query('ROLLBACK');
    //         result(e, null);
    //         throw e
    //     } finally {
    //         client.release()
    //     }
    //     result(null, "success");
    // })().catch(e => console.error(e.stack));


    pool.query("SELECT nextval('seq_wilayah');", (error, results) => {

        if (error) {
            throw error;
        }
        seqId = results.rows[0].nextval;
        const custArea = desaId + ("0" + seqId).slice(-2);

        pool.query("INSERT INTO public.mt_wilayah(" +
            " id, wilayah_name, , desa_id, desa_name, kecamatan_id, kecamatan_name, kota_id, kota_name, provinsi_id, provinsi_name, area_id, longitude, latitude, created_date, last_update, flag, action)" +
            " VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, $13, now(), now(), 'Y', 'C');" , [seqId, wilayahName, desaId, desaName, kecamatanId, kecamatanName, kotaId, kotaName, provinsiId, provinsiName, custArea, longitude, latitude], (error, results) => {
                console.log(seqId +" - "+ wilayahName +" - "+desaId +" - "+desaName +" - "+kecamatanId +" - "+kecamatanName +" - "+kotaId +" - "+kotaName +" - "+provinsiId +" - "+provinsiName +" - "+custArea +" - "+longitude +" - "+latitude)
                console.log("==== 3 ==== " + error);
                if (error) {
                    result(error, null);
                }
                result(null, "success");
            })
    })

}

const updateWilayah = (request, result) => {
    const { id, wilayahName, desaId, desaName, kecamatanId, kecamatanName, kotaId, kotaName, provinsiId, provinsiName, areaId, longitude, latitude } = request.body;
    const byWho = request.cookies.username;

    pool.query(
        "UPDATE mt_wilayah" +
        " SET wilayah_name = $2, desa_id = $3, desa_name = $4, kecamatan_id = $5, kecamatan_name = $6, kota_id = $7, kota_name = $8, provinsi_id = $9, provinsi_name = $10, area_id = $11, longitude = $12, latitude = $13, last_update = now(), action = 'U'" +
        " WHERE id = $1",
        [id, wilayahName, desaId, desaName, kecamatanId, kecamatanName, kotaId, kotaName, provinsiId, provinsiName, areaId, longitude, latitude],
        (error, results) => {
            if (error) {
                result(error, null);
            }
            result(null, "success");
        }
    )
}

const deleteWilayah = (request, result) => {
    const id = request.body.id;
    const byWho = request.cookies.username;

    pool.query("UPDATE mt_wilayah SET flag = 'N', action = 'D', last_update = now(), last_update_who = $2 WHERE id = $1", [id, byWho], (error, results) => {
        if (error) {
            result(error, null);
        }
        result(null, "success");
    })
}

module.exports = { getWilayah, getWilayahById, getWilayahByArea, insertWilayah, updateWilayah, deleteWilayah };