const { pool } = require("../config/database.js");


const getKecamatan = (result) => {
    pool.query("SELECT * FROM im_kecamatan WHERE flag = 'Y' ORDER BY kecamatan_id ASC", (error, results) => {
        if (error) {
            result(error,null);
        }
        result(null, results.rows);
    })
}

const getKecamatanById = (request, result) => {
    const id = request.params.id;

    pool.query("SELECT * FROM im_kecamatan WHERE flag = 'Y' AND kecamatan_id = $1", [id], (error, results) => {
        if (error) {
            result(error, null);
        }
        result(null, results.rows);
    })
}

const getKecamatanByKota = (request, result) => {
    const id = request.params.id;

    pool.query("SELECT * FROM im_kecamatan WHERE flag = 'Y' AND kota_id = $1", [id], (error, results) => {
        if (error) {
            result(error, null);
        }
        result(null, results.rows);
    })
}

module.exports = { getKecamatan, getKecamatanById, getKecamatanByKota };