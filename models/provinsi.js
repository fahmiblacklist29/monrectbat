const { pool } = require("../config/database.js");


const getProvinsi = (result) => {
    pool.query("SELECT * FROM im_provinsi WHERE flag = 'Y' ORDER BY provinsi_id ASC", (error, results) => {
        if (error) {
            result(error,null);
        }
        result(null, results.rows);
    })
}

const getProvinsiById = (request, result) => {
    const id = request.params.id;

    pool.query("SELECT * FROM im_provinsi WHERE flag = 'Y' AND provinsi_id = $1", [id], (error, results) => {
        if (error) {
            result(error, null);
        }
        result(null, results.rows);
    })
}

module.exports = { getProvinsi, getProvinsiById };