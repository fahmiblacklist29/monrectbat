const { pool } = require("../config/database.js");


const getRectifierLog = (result) => {
    pool.query("SELECT * FROM it_rectifier_log WHERE flag = 'Y' ORDER BY id ASC", (error, results) => {
        if (error) {
            result(error,null);
        }
        result(null, results.rows);
    })
}

const getRectifierLogById = (request, result) => {
    const id = request.params.id;

    pool.query("SELECT * FROM it_rectifier_log WHERE flag = 'Y' AND id = $1", [id], (error, results) => {
        if (error) {
            result(error, null);
        }
        result(null, results.rows);
    })
}

const getRectifierLogBySN = (request, result) => {
    const id = request.params.id;

    pool.query("SELECT * FROM it_rectifier_log WHERE flag = 'Y' AND serial_number = $1", [id], (error, results) => {
        if (error) {
            result(error, null);
        }
        result(null, results.rows);
    })
}

const getRectifierLogByDate = (request, result) => {
    const datefrom = request.params.datefrom;
    const dateto = request.params.dateto;


    pool.query("SELECT * FROM it_rectifier_log WHERE flag = 'Y' AND create_date BETWEEN $1 AND $2", [datefrom, dateto], (error, results) => {
        if (error) {
            result(error, null);
        }
        result(null, results.rows);
    })
}

const getFilteredLog = (request, result) => {
    const { sn, beginDate, endDate } = request.body;

    pool.query("SELECT "+
        "max(a.voltage_load) AS voltage_load,"+
        "max(a.current_load) as current_load,"+
        "max(a.voltage_battery) as voltage_battery,"+
        "max(a.current_battery) as current_battery,"+
        "max(a.voltage_ln) as voltage_ln,"+
        "max(a.current_l) as current_l,"+
        "max(a.room_temp) as room_temp,"+
        "max(a.panel_temp) as panel_temp,"+
        "to_char(a.created_date, 'yyyy-mm-dd HH24:MI') as jam,"+
        "to_char(max(a.created_date), 'yyyy-mm-dd HH24:MI:SS') as tanggal "+
        "FROM it_rectifier_log as a "+
        "WHERE a.serial_number = $1 and "+
        "a.created_date between $2 AND $3 "+
        "GROUP BY jam ORDER BY jam ;", [sn, beginDate, endDate], (error, results) => {

        if (error) {
            result(error, null);
        }else if(results.rowCount>0){
            result(null, results.rows);
        }
    })
}

module.exports = { getRectifierLog, getRectifierLogById, getRectifierLogBySN, getRectifierLogByDate, getFilteredLog };