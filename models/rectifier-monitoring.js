const { pool } = require("../config/database.js");


const getRectifierMonitoring = (result) => {
    pool.query("SELECT * FROM mt_rectifier_monitoring WHERE flag = 'Y' ORDER BY id ASC", (error, results) => {
        if (error) {
            result(error,null);
        }
        result(null, results.rows);
    })
}

const getRectifierMonitoringById = (request, result) => {
    const id = request.params.id;

    pool.query("SELECT * FROM mt_rectifier_monitoring WHERE flag = 'Y' AND id = $1", [id], (error, results) => {
        if (error) {
            result(error, null);
        }
        result(null, results.rows);
    })
}

const getRectifierMonitoringBySN = (request, result) => {
    const id = request.params.id;

    pool.query("SELECT * FROM mt_rectifier_monitoring WHERE flag = 'Y' AND serial_number = $1", [id], (error, results) => {
        if (error) {
            result(error, null);
        }
        result(null, results.rows);
    })
}

module.exports = { getRectifierMonitoring, getRectifierMonitoringById, getRectifierMonitoringBySN };