const { pool } = require("../config/database.js");


const getEventLog = (result) => {
    pool.query("SELECT a.*, TO_CHAR(a.created_date, 'Day, DD-Mon-YYYY HH24:MI') AS date_time, b.rectifier_id FROM it_event_log a " +
        " LEFT JOIN im_rectifier b ON b.serial_number = a.serial_number " +
        " ORDER BY a.created_date DESC", (error, results) => {
            if (error) {
                result(error, null);
            }
            result(null, results.rows);
        })
}

const getEventLogByDate = (request, result) => {
    const { beginDate, endDate } = request.body;

    pool.query("SELECT a.*, TO_CHAR(a.created_date, 'Day, DD-Mon-YYYY HH24:MI') AS date_time, b.rectifier_id FROM it_event_log a " +
        " LEFT JOIN im_rectifier b ON b.serial_number = a.serial_number " +
        " WHERE a.created_date BETWEEN $1 AND $2 " +
        " ORDER BY a.created_date DESC", [beginDate, endDate], (error, results) => {
            if (error) {
                result(error, null);
            }
            result(null, results.rows);
        })
}

const getEventLogById = (request, result) => {
    const id = request.params.id;

    pool.query("SELECT a.*, TO_CHAR(a.created_date, 'Day, DD-Mon-YYYY HH24:MI') AS date_time, b.rectifier_id FROM it_event_log a " +
        " LEFT JOIN im_rectifier b ON b.serial_number = a.serial_number " +
        " WHERE a.id = $1 ORDER BY a.created_date DESC", [id], (error, results) => {
            if (error) {
                result(error, null);
            }
            result(null, results.rows);
        })
}

const getEventLogBySN = (request, result) => {
    const id = request.params.id;

    pool.query("SELECT a.*, TO_CHAR(a.created_date, 'Day, DD-Mon-YYYY HH24:MI') AS date_time FROM it_event_log a " +
        " LEFT JOIN im_rectifier b ON b.serial_number = a.serial_number " +
        " WHERE a.serial_number = $1 ORDER BY a.created_date DESC", [id], (error, results) => {
            if (error) {
                result(error, null);
            }
            result(null, results.rows);
        })
}

const getEventLogByType = (request, result) => {
    const type = request.params.type;

    pool.query("SELECT a.*, TO_CHAR(a.created_date, 'Day, DD-Mon-YYYY HH24:MI') AS date_time FROM it_event_log a " +
        " LEFT JOIN im_rectifier b ON b.serial_number = a.serial_number " +
        " WHERE a.event_type = $1 ORDER BY a.created_date DESC", [type], (error, results) => {
            if (error) {
                result(error, null);
            }
            result(null, results.rows);
        })
}

const getEventLogByStatus = (request, result) => {
    const stat = request.params.stat;

    pool.query("SELECT a.*, TO_CHAR(a.created_date, 'Day, DD-Mon-YYYY HH24:MI') AS date_time FROM it_event_log a " +
        " LEFT JOIN im_rectifier b ON b.serial_number = a.serial_number " +
        " WHERE a.status = $1 ORDER BY a.created_date DESC", [stat], (error, results) => {
            if (error) {
                result(error, null);
            }
            result(null, results.rows);
        })
}

const getEventLogByLocation = (request, result) => {
    const location = request.params.location;

    pool.query("SELECT a.*, TO_CHAR(a.created_date, 'Day, DD-Mon-YYYY HH24:MI') AS date_time FROM it_event_log a" +
        " LEFT JOIN im_rectifier b ON b.serial_number = a.serial_number " +
        " WHERE a.location = $1 ORDER BY a.created_date DESC", [location], (error, results) => {
            if (error) {
                result(error, null);
            }
            result(null, results.rows);
        })
}

module.exports = { getEventLog, getEventLogById, getEventLogBySN, getEventLogByType, getEventLogByStatus, getEventLogByLocation, getEventLogByDate };