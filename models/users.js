const {pool} = require("../config/database.js");
const md5 = require("md5");

const getUsers = (result) => {
    pool.query("SELECT * FROM im_user WHERE flag = 'Y' ORDER BY id ASC", (error, results) => {
        if (error) {
            result(error, null);
        }
        result(null, results.rows);
    })
}

const getUserById = (request, result) => {
    const id = request.params.id;

    pool.query('SELECT * FROM im_user WHERE id = $1', [id], (error, results) => {
        if (error) {
            result(error, null);
        }
        result(null, results.rows);
    })
}

const getUserWithWilayahById = (request, result) => {
    const id = request.params.id;

    pool.query('SELECT a.*, b.desa_id, b.desa_name, b.kecamatan_id, b.kecamatan_name, b.kota_id, b.kota_name, b.provinsi_id, b.provinsi_name, b.longitude, b.latitude FROM im_user a' +
    ' LEFT JOIN mt_wilayah b ON a.area_id = b.id' +
    ' WHERE a.id = $1', [id], (error, results) => {
        if (error) {
            result(error, null);
        }
        result(null, results.rows);
    })
}
const createUser = async (request, result) => {
    const { name, username, password, idRole, area, koordinat, areaId  } = request.body;
    const byWho = request.cookies.username;
    var seqId;
    const encPwrd = md5(password);

    pool.query("SELECT nextval('seq_user');",(error,results)=>{

        if(error){
            throw error;
        }

        seqId = results.rows[0].nextval;
        pool.query("INSERT INTO im_user(" +
            " id, action, flag, active, name, password, username, id_role, created_date, created_who, last_update, last_update_who, area, koordinat, area_id, photo)" +
            " VALUES ($1, 'C', 'Y', true, $2, $3, $4, $5, now(), $6, now(), $6, $7, $8, $9, 'default.png');",
            [seqId, name, encPwrd, username, idRole, byWho, area, koordinat, areaId], (error, results) => {
            if (error) {
                result(error, null);
            }
            result(null, "success");
        })
    })
   
}

const updateUser = (request, result) => {
    const { id, name, username, password, idRole, area, koordinat, areaId, ischangepass } = request.body;
    const byWho = request.cookies.username;

    if("" != password && password != null && ischangepass == 'Y'){
        const encPwrd = md5(password);
        pool.query(
            "UPDATE im_user"+
            " SET name = $1, action = 'U', password = $2, username = $3, id_role = $4, last_update = now(), last_update_who = $5, area = $6, koordinat = $7, area_id = $8 " +
            " WHERE id = $9",
            [name, encPwrd, username, idRole, byWho, area, koordinat, areaId, id],
            (error, results) => {
                if (error) {
                    result(error, null);
                }
                result(null, "success");
            }
        )
    }else{
        pool.query(
            "UPDATE im_user SET name = $1, action = 'U', username = $2, id_role = $3, last_update = now(), last_update_who = $4, area = $5, koordinat = $6, area_id = $7 WHERE id = $8",
            [name, username, idRole, byWho, area, koordinat, areaId, id],
            (error, results) => {
                if (error) {
                    result(error, null);
                }
                result(null, "success");
            }
        )
    }
    
}

const deleteUser = (request, result) => {
    const id = request.body.id;
    const byWho = request.cookies.username;

    pool.query("UPDATE im_user SET flag = 'N', action = 'D', active = false, last_update = now(), last_update_who = $2 WHERE id = $1", [id,byWho], (error, results) => {
        if (error) {
            result(error, null);
        }
        result(null, "success");
    })
}

module.exports = { getUsers, getUserById, getUserWithWilayahById, createUser, updateUser, deleteUser };