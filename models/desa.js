const { pool } = require("../config/database.js");


const getDesa = (result) => {
    pool.query("SELECT * FROM im_desa WHERE flag = 'Y' ORDER BY desa_id ASC", (error, results) => {
        if (error) {
            result(error,null);
        }
        result(null, results.rows);
    })
}

const getDesaById = (request, result) => {
    const id = request.params.id;

    pool.query("SELECT * FROM im_desa WHERE flag = 'Y' AND desa_id = $1", [id], (error, results) => {
        if (error) {
            result(error, null);
        }
        result(null, results.rows);
    })
}

const getDesaByKecamatan = (request, result) => {
    const id = request.params.id;

    pool.query("SELECT * FROM im_desa WHERE flag = 'Y' AND kecamatan_id = $1", [id], (error, results) => {
        if (error) {
            result(error, null);
        }
        result(null, results.rows);
    })
}

module.exports = { getDesa, getDesaById, getDesaByKecamatan };