const { pool } = require("../config/database.js");

const getRectifier = (result) => {
    pool.query("SELECT a.rectifier_id, a.rectifier_name, a.serial_number, a.lat, a.\"long\", a.desa_id, a.kecamatan_id, a.kota_id, a.provinsi_id, a.mqtt_topic, a.description, " +
        " b.desa_name, c.kecamatan_name, d.kota_name,  e.provinsi_name, " +
        " CASE WHEN (f.last_update IS null) OR (f.last_update < (now() - interval '2 minute')) THEN 'disconnect' " +
        "   WHEN g.alarm > 0 THEN 'alarm' " +
        "   ELSE 'connect' END AS status " +
        " FROM im_rectifier a" +
        " LEFT JOIN im_desa b ON b.desa_id = a.desa_id" +
        " LEFT JOIN im_kecamatan c ON c.kecamatan_id = a.kecamatan_id" +
        " LEFT JOIN im_kota d ON d.kota_id = a.kota_id" +
        " LEFT JOIN im_provinsi e ON e.provinsi_id = a.provinsi_id" +
        " LEFT JOIN mt_rectifier_monitoring f ON f.serial_number = a.serial_number" +
        " LEFT JOIN (" +
        "    SELECT serial_number, COUNT(flag_notif) AS alarm " +
        "    FROM it_event_log " +
        "    WHERE flag_notif = 'Y' AND event_type = 'alarm' " +
        "    GROUP BY serial_number) g ON g.serial_number = a.serial_number" +
        " WHERE a.flag = 'Y' ORDER BY a.rectifier_id ASC", (error, results) => {

            if (error) {
                result(error, null);
            }

            if (results.rowCount > 0) {
                result(null,
                    results.rows.sort((a, b) => {
                        return a.kota_id.localeCompare(b.kota_id);
                    }));
            } else {
                result(null, JSON.parse("[]"));
            }

        })
}

const getRectifierList = () => {
    pool.query("SELECT a.*, b.desa_name, c.kecamatan_name, d.kota_name, e.provinsi_name," +
        " CASE WHEN (f.last_update IS null) OR (f.last_update < (now() - interval '2 minute')) THEN 'disconnect' " +
        "   WHEN g.alarm > 0 THEN 'alarm' " +
        "   ELSE 'connect' END AS status " +
        " FROM im_rectifier a" +
        " LEFT JOIN im_desa b ON b.desa_id = a.desa_id" +
        " LEFT JOIN im_kecamatan c ON c.kecamatan_id = a.kecamatan_id" +
        " LEFT JOIN im_kota d ON d.kota_id = a.kota_id" +
        " LEFT JOIN im_provinsi e ON e.provinsi_id = a.provinsi_id" +
        " LEFT JOIN mt_rectifier_monitoring f ON f.serial_number = a.serial_number" +
        " LEFT JOIN (" +
        "    SELECT serial_number, COUNT(flag_notif) AS alarm " +
        "    FROM it_event_log " +
        "    WHERE flag_notif = 'Y' AND event_type = 'alarm' " +
        "    GROUP BY serial_number) g ON g.serial_number = a.serial_number" +
        " WHERE a.flag = 'Y' ORDER BY a.rectifier_id ASC", (error, results) => {
            if (error) {
                throw (error);
            }
            if (results.rowCount > 0) {
                return results.rows.sort((a, b) => {
                    return a.kota_id.localeCompare(b.kota_id);
                });
            } else {
                return JSON.parse("[]");
            }

        })
}

const getRectifierWithLimit = (name, result) => {
    
    pool.query("SELECT a.*, b.desa_name, c.kecamatan_name, d.kota_name, e.provinsi_name," +
        " CASE WHEN (f.last_update IS null) OR (f.last_update < (now() - interval '2 minute')) THEN 'disconnect' " +
        "   WHEN g.alarm > 0 THEN 'alarm' " +
        "   ELSE 'connect' END AS status " +
        " FROM im_rectifier a" +
        " LEFT JOIN im_desa b ON b.desa_id = a.desa_id" +
        " LEFT JOIN im_kecamatan c ON c.kecamatan_id = a.kecamatan_id" +
        " LEFT JOIN im_kota d ON d.kota_id = a.kota_id" +
        " LEFT JOIN im_provinsi e ON e.provinsi_id = a.provinsi_id" +
        " LEFT JOIN mt_rectifier_monitoring f ON f.serial_number = a.serial_number" +
        " LEFT JOIN (" +
        "    SELECT serial_number, COUNT(flag_notif) AS alarm " +
        "    FROM it_event_log " +
        "    WHERE flag_notif = 'Y' AND event_type = 'alarm' " +
        "    GROUP BY serial_number) g ON g.serial_number = a.serial_number" +
        " WHERE a.flag = 'Y' AND a.rectifier_name ILIKE '%"+name+"%' ORDER BY a.rectifier_id ASC LIMIT 5", (error, results) => {
            
            if (error) {
                
                //throw (error);
                result(error, null);
            }
            if (results.rowCount > 0) {
                
                result(null, results.rows);
                // return results.rows.sort((a, b) => {
                //     return a.kota_id.localeCompare(b.kota_id);
                // });
            } else {
                
                result(null, JSON.parse("[]"));
                //return JSON.parse("[]");
            }

        })
}

const getRectifierById = (request, result) => {
    const id = request.params.id;

    pool.query("SELECT a.*, b.desa_name, c.kecamatan_name, d.kota_name, e.provinsi_name, rt.*, " +
        " TO_CHAR(rt.last_update, 'Day, DD-Month-YYYY HH24:MI:SS')  AS last_update_data, rt.last_update > (now() - interval '2 minute') AS connect" +
        " FROM im_rectifier a" +
        " LEFT JOIN mt_rectifier_monitoring rt ON rt.serial_number = a.serial_number" +
        " LEFT JOIN im_desa b ON b.desa_id = a.desa_id" +
        " LEFT JOIN im_kecamatan c ON c.kecamatan_id = a.kecamatan_id" +
        " LEFT JOIN im_kota d ON d.kota_id = a.kota_id" +
        " LEFT JOIN im_provinsi e ON e.provinsi_id = a.provinsi_id" +
        " WHERE a.flag = 'Y' AND a.rectifier_id = $1", [id], (error, results) => {
            if (error) {
                result(error, null);
            }

            result(null, results.rows);
        })
}

const getRectifierBySN = (request, result) => {
    const id = request.params.id;

    pool.query("SELECT a.*, b.desa_name, c.kecamatan_name, d.kota_name, e.provinsi_name FROM im_rectifier a" +
        " LEFT JOIN im_desa b ON b.desa_id = a.desa_id" +
        " LEFT JOIN im_kecamatan c ON c.kecamatan_id = a.kecamatan_id" +
        " LEFT JOIN im_kota d ON d.kota_id = a.kota_id" +
        " LEFT JOIN im_provinsi e ON e.provinsi_id = a.provinsi_id" +
        " WHERE a.flag = 'Y' AND a.serial_number = $1", [id], (error, results) => {
            if (error) {
                result(error, null);
            }
            result(null, results.rows);
        })
}

const getRectifierByProvinsi = (request, result) => {
    const id = request.params.id;

    pool.query("SELECT a.*, b.desa_name, c.kecamatan_name, d.kota_name, e.provinsi_name FROM im_rectifier a" +
        " LEFT JOIN im_desa b ON b.desa_id = a.desa_id" +
        " LEFT JOIN im_kecamatan c ON c.kecamatan_id = a.kecamatan_id" +
        " LEFT JOIN im_kota d ON d.kota_id = a.kota_id" +
        " LEFT JOIN im_provinsi e ON e.provinsi_id = a.provinsi_id" +
        " WHERE a.flag = 'Y' AND a.provinsi_id = $1", [id], (error, results) => {
            if (error) {
                result(error, null);
            }
            result(null, results.rows);
        })
}

const getRectifierByKota = (request, result) => {
    const id = request.params.id;

    pool.query("SELECT a.*, b.desa_name, c.kecamatan_name, d.kota_name, e.provinsi_name FROM im_rectifier a" +
        " LEFT JOIN im_desa b ON b.desa_id = a.desa_id" +
        " LEFT JOIN im_kecamatan c ON c.kecamatan_id = a.kecamatan_id" +
        " LEFT JOIN im_kota d ON d.kota_id = a.kota_id" +
        " LEFT JOIN im_provinsi e ON e.provinsi_id = a.provinsi_id" +
        " WHERE a.flag = 'Y' AND a.kota_id = $1", [id], (error, results) => {
            if (error) {
                result(error, null);
            }
            result(null, results.rows);
        })
}


const insertRectifier = (request, result) => {
    const { name, lat, lon, desa, kecamatan, kota, provinsi, mqttTopic, serialNumber, description } = request.body;
    const byWho = request.cookies.username;
    var seqId;

    pool.query("SELECT nextval('seq_rectifier');", (error, results) => {
        if (error) {
            throw error;
        }
        seqId = results.rows[0].nextval;
    })
    pool.query("INSERT INTO im_rectifier(" +
        ' rectifier_id, rectifier_name, lat, "long", desa_id, kecamatan_id, kota_id, provinsi_id, mqtt_topic, serial_number, created_who, description, created_date, last_update_who, last_update, flag, action)' +
        " VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, now(), $11, now(), 'Y', 'C');", [seqId, name, lat, lon, desa, kecamatan, kota, provinsi, mqttTopic, serialNumber, byWho, description], (error, results) => {
            if (error) {
                result(error, null);
            }
            result(null, "success");
        })
}

const updateRectifier = (request, result) => {
    const { id, name, lat, lon, desa, kecamatan, kota, provinsi, mqttTopic, serialNumber, description } = request.body;
    const byWho = request.cookies.username;

    pool.query("UPDATE im_rectifier " +
        " SET rectifier_name = $2, serial_number = $3, lat = $4, \"long\"= $5, desa_id = $6, kecamatan_id = $7, kota_id = $8, provinsi_id = $9, mqtt_topic = $10, last_update_who = $11, description = $12, last_update = now(), action = 'U'" +
        " WHERE rectifier_id = $1 ;", [id, name, serialNumber, lat, lon, desa, kecamatan, kota, provinsi, mqttTopic, byWho, description], (error, results) => {
            if (error) {
                result(error, null);
            }
            result(null, "success");
        })
}

const updateRectiName = (request, result) => {
    const { id, name, desc } = request.body;
    const byWho = request.cookies.username;

    pool.query("UPDATE im_rectifier " +
        " SET rectifier_name = $2, description = $3, last_update_who = $4, last_update = now(), action = 'U'" +
        " WHERE rectifier_id = $1 ;", [id, name, desc, byWho], (error, results) => {
            if (error) {
                result(error, null);
            }
            result(null, "success");
        })
}

const deleteRectifier = (request, result) => {
    const id = request.body.id;
    const byWho = request.cookies.username;

    pool.query("UPDATE im_rectifier " +
        " SET last_update_who = $2, last_update = now(), flag = 'N', action = 'D'" +
        " WHERE rectifier_id = $1 ;", [id, byWho], (error, results) => {
            if (error) {
                result(error, null);
            }
            result(null, "success");
        })
}

module.exports = { getRectifier, getRectifierById, updateRectiName, getRectifierBySN, getRectifierByProvinsi, getRectifierByKota, insertRectifier, updateRectifier, deleteRectifier, getRectifierList, getRectifierWithLimit };