const { pool } = require("../config/database.js");


const getUserTelegram = (result) => {
    pool.query("SELECT * FROM im_user_telegram WHERE flag = 'Y' ORDER BY id ASC", (error, results) => {
        if (error) {
            result(error, null);
        }
        
        result(null, results.rows);
    })
}

const getUserTelegramById = (request, result) => {
    const id = request.params.id;

    pool.query("SELECT * FROM im_user_telegram WHERE flag = 'Y' AND id = $1", [id], (error, results) => {
        if (error) {
            result(error, null);
        }
        result(null, results.rows);
    })
}

const getUserTelegramByChatId = (request, result) => {
    const chatId = request.body.chatId;

    pool.query("SELECT * FROM im_user_telegram WHERE flag = 'Y' AND chat_id = $1", [chatId], (error, results) => {
        if (error) {
            result(error, null);
        }
        result(null, results.rows);
    })
}

const insertUserTelegram = async (request, result) => {
    const { name, chatId } = request.body;
    var seqId;

    pool.query("SELECT nextval('seq_user_telegram');", (error, results) => {

        if (error) {
            throw error;
        }
        seqId = ("0" + results.rows[0].nextval).slice(-2);

        pool.query("INSERT INTO im_user_telegram(" +
            " id, name, chat_id, action, flag, created_date, last_update)" +
            " VALUES ($1, $2, $3, 'C', 'Y', now(), now() );", [seqId, name, chatId], (error, results) => {
                if (error) {
                    result(error, null);
                }
                result(null, "success");
            })
    })

}

const updateUserTelegram = (request, result) => {
    const { chatId, name} = request.body;

    pool.query(
        "UPDATE im_user_telegram " +
        " SET name = $2, action = 'U', last_update = now() " +
        " WHERE chat_id = $1; ",
        [chatId, name, token],
        (error, results) => {
            if (error) {
                result(error, null);
            }
            result(null, "success");
        }
    )
}

// const deleteUserTelegram = (request, result) => {
//     const chatId = request.body.chatId;

//     pool.query("UPDATE im_user_telegram SET flag = 'N', action = 'D', last_update = now() WHERE chat_id = $1", [chatId], (error, results) => {
//         if (error) {
//             result(error, null);
//         }
//         result(null, "success");
//     })
// }

const deleteUserTelegram = (request, result) => {
    const chatId = request.body.chatId;

    pool.query("DELETE FROM im_user_telegram WHERE chat_id = $1", [chatId], (error, results) => {
        if (error) {
            result(error, null);
        }
        result(null, "success");
    })
}

module.exports = { getUserTelegram, getUserTelegramById, getUserTelegramByChatId, insertUserTelegram, updateUserTelegram, deleteUserTelegram };