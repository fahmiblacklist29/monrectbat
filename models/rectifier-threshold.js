const { Pool } = require("pg");
const { pool } = require("../config/database.js");


const getRectifierthreshold = (result) => {
    pool.query("SELECT * FROM mt_rectifier_threshold WHERE flag = 'Y' ORDER BY id ASC", (error, results) => {
        if (error) {
            result(error, null);
        }
        result(null, results.rows);
    })
}

const getRectifierthresholdById = (request, result) => {
    const id = request.params.id;

    pool.query("SELECT * FROM mt_rectifier_threshold WHERE flag = 'Y' AND id = $1", [id], (error, results) => {
        if (error) {
            result(error, null);
        }
        result(null, results.rows);
    })
}

const getRectifierthresholdBySN = (request, result) => {
    const sn = request.params.sn;

    pool.query("SELECT * FROM mt_rectifier_threshold WHERE flag = 'Y' AND serial_number = $1", [sn], (error, results) => {
        if (error) {
            result(error, null);
        }
        result(null, results.rows);
    })
}

const insertRectifierThreshold = (request, result) => {
    const { serialNumber, voltLoadMin, voltLoadMax, curLoadMin, curLoadMax, voltBattMin, voltBattMax, curBattMin, curBattMax, voltLnMin, voltLnMax, curLMin, curLMax, roomTempMin, roomTempMax, panelTempMin, panelTempMax } = request.body;
    var seqId;
    const byWho = request.cookies.username;

    pool.query("SELECT nextval('seq_rectifier_threshold');", (error, results) => {
        if (error) {
            throw error;
        }
        seqId = results.rows[0].nextval;
    })
    pool.query("INSERT INTO public.mt_rectifier_threshold(" +
        " id, serial_number, voltage_load_min, voltage_load_max, current_load_min, current_load_max, voltage_batt_min, voltage_batt_max, current_batt_min, current_batt_max, voltage_ln_min, voltage_ln_max, current_l_min, current_l_max, room_temp_min, room_temp_max, panel_temp_min, panel_temp_max, created_date, last_update, flag, action, last_update_who) " +
        " VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, $13, $14, $15, $16, $17, $18, now(), now(), 'Y', 'C', $19);", [seqId, serialNumber, voltLoadMin, voltLoadMax, curLoadMin, curLoadMax, voltBattMin, voltBattMax, curBattMin, curBattMax, voltLnMin, voltLnMax, curLMin, curLMax, roomTempMin, roomTempMax, panelTempMin, panelTempMax, byWho], (error, results) => {
            // console.log(results);
            if (error) {
                result(error, null);
            }
            result(null, "success");
        })
}

const updateRectifierThreshold = (request, result) => {
    const { threshId, serialNumber, voltLoadMin, voltLoadMax, curLoadMin, curLoadMax, voltBattMin, voltBattMax, curBattMin, curBattMax, voltLnMin, voltLnMax, curLMin, curLMax, roomTempMin, roomTempMax, panelTempMin, panelTempMax } = request.body;
    const byWho = request.cookies.username;

    var pool = new Pool();
    pool.query("UPDATE mt_rectifier_threshold" +
        " SET serial_number = $2, voltage_load_min = $3, voltage_load_max = $4, current_load_min = $5, current_load_max = $6, voltage_batt_min = $7, voltage_batt_max = $8, current_batt_min = $9, current_batt_max = $10, voltage_ln_min = $11, voltage_ln_max = $12, current_l_min = $13, current_l_max = $14, room_temp_min = $15, room_temp_max = $16, panel_temp_min = $17, panel_temp_max = $18, last_update_who = $19, last_update = now(), action = 'C'" +
        " WHERE id = $1", [threshId, serialNumber, voltLoadMin, voltLoadMax, curLoadMin, curLoadMax, voltBattMin, voltBattMax, curBattMin, curBattMax, voltLnMin, voltLnMax, curLMin, curLMax, roomTempMin, roomTempMax, panelTempMin, panelTempMax, byWho], (error, results) => {
            if (error) {
                result(error, null);
            }
            result(null, "success");
        })
}

const deleteRectifierThreshold = (request, result) => {
    const id = request.body.id;
    const byWho = request.cookies.username;

    pool.query("UPDATE mt_rectifier_threshold " +
        " SET last_update_who = $2, last_update = now(), flag = 'N', action = 'D'" +
        " WHERE id = $1 ;", [id, byWho], (error, results) => {
            if (error) {
                result(error, null);
            }
            result(null, "success");
        })
}

const updateThreshInput = (request, result) => {
    const { sn, vmin, vmax, imin, imax } = request.body;
    const byWho = request.cookies.username;

    pool.query("UPDATE mt_rectifier_threshold " +
        " SET voltage_ln_min = $2, voltage_ln_max = $3, current_l_min = $4, current_l_max = $5, last_update_who = $6, last_update = now(), action = 'U'" +
        " WHERE serial_number = $1 ;", [sn, vmin, vmax, imin,imax, byWho], (error, results) => {
            if (error) {
                result(error, null);
            }
            result(null, "success");
        })
}

const updateThreshLoad = (request, result) => {
    const { sn, vmin, vmax, imin, imax } = request.body;
    const byWho = request.cookies.username;

    pool.query("UPDATE mt_rectifier_threshold " +
        " SET voltage_load_min = $2, voltage_load_max = $3, current_load_min = $4, current_load_max = $5, last_update_who = $6, last_update = now(), action = 'U'" +
        " WHERE serial_number = $1 ;", [sn, vmin, vmax, imin, imax, byWho], (error, results) => {
            if (error) {
                result(error, null);
            }
            result(null, "success");
        })
}

const updateThreshBatt = (request, result) => {
    const { sn, vmin, vmax, imin, imax } = request.body;
    const byWho = request.cookies.username;

    pool.query("UPDATE mt_rectifier_threshold " +
        " SET voltage_batt_min = $2, voltage_batt_max = $3, current_batt_min = $4, current_batt_max = $5, last_update_who = $6, last_update = now(), action = 'U'" +
        " WHERE serial_number = $1 ;", [sn, vmin, vmax, imin, imax, byWho], (error, results) => {
            if (error) {
                result(error, null);
            }
            result(null, "success");
        })
}

const updateRoomTemp = (request, result) => {
    const { sn, min, max } = request.body;
    const byWho = request.cookies.username;

    pool.query("UPDATE mt_rectifier_threshold " +
        " SET room_temp_min = $2, room_temp_max = $3, last_update_who = $4, last_update = now(), action = 'U'" +
        " WHERE serial_number = $1 ;", [sn, min, max, byWho], (error, results) => {
            if (error) {
                result(error, null);
            }
            result(null, "success");
        })
}

const updatePanelTemp = (request, result) => {
    const { sn, min, max } = request.body;
    const byWho = request.cookies.username;

    pool.query("UPDATE mt_rectifier_threshold " +
        " SET panel_temp_min = $2, panel_temp_max = $3, last_update_who = $4, last_update = now(), action = 'U'" +
        " WHERE serial_number = $1 ;", [sn, min, max, byWho], (error, results) => {
            if (error) {
                result(error, null);
            }
            result(null, "success");
        })
}


module.exports = { getRectifierthreshold, getRectifierthresholdById, getRectifierthresholdBySN, insertRectifierThreshold, updateRectifierThreshold, deleteRectifierThreshold, updateThreshInput, updateThreshLoad, updateThreshBatt, updateRoomTemp, updatePanelTemp };