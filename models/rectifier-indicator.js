const { pool } = require("../config/database.js");


const getRectifierIndicator = (result) => {
    pool.query("SELECT * FROM mt_rectifier_indicator WHERE flag = 'Y' ORDER BY id ASC", (error, results) => {
        if (error) {
            result(error, null);
        }
        result(null, results.rows);
    })
}

const getRectifierIndicatorById = (request, result) => {
    const id = request.params.id;

    pool.query("SELECT * FROM mt_rectifier_indicator WHERE flag = 'Y' AND id = $1", [id], (error, results) => {
        if (error) {
            result(error, null);
        }
        result(null, results.rows);
    })
}

const getRectifierIndicatorBySN = (request, result) => {
    const sn = request.params.sn;

    pool.query("SELECT * FROM mt_rectifier_indicator WHERE flag = 'Y' AND serial_number = $1 ORDER BY channel", [sn], (error, results) => {
        if (error) {
            result(error, null);
        }
        result(null, results.rows);
    })
}

const getRectifierIndicatorBySnAndChannel = (request, result) => {
    const id = request.params.id;
    const ch = request.params.ch;


    pool.query("SELECT * FROM mt_rectifier_indicator WHERE flag = 'Y' AND serial_number = $1 AND channel = $2", [id, ch], (error, results) => {
        if (error) {
            result(error, null);
        }
        if (results.rowCount > 0) {
            result(null, results.rows);
        } else {
            result(null, JSON.parse("[]"));
        }
    })
}






const getRectifierIndicatorBySnAndEvent = (request, result) => {
    const id = request.params.id;
    const type = request.params.ch;


    pool.query("SELECT * FROM mt_rectifier_indicator WHERE flag = 'Y' AND serial_number = $1 AND event_type = $2", [id, type], (error, results) => {
        if (error) {
            result(error, null);
        }
        result(null, results.rows);
    })
}

const insertRectifierIndicator = (request, result) => {
    const { sn, data } = request.body;
    var seqId;
    let d = JSON.parse(data);

    d.forEach(indicator => {

        if (indicator.flag == "C") {

            pool.query("SELECT nextval('seq_rectifier_indicator');", (error, results) => {
                if (error) {
                    throw error;
                }
                seqId = results.rows[0].nextval;


                pool.query("INSERT INTO mt_rectifier_indicator(" +
                    " id, serial_number, channel, name, color, event_type, created_date, last_update, flag, action)" +
                    " VALUES ($1, $2, $3, $4, $5, $6, now(), now(), 'Y', 'C');", [seqId, sn, indicator.channel, indicator.name, indicator.color, indicator.event], (error, results) => {
                        // To do Something

                        if (error) {
                            result(error, null);
                        }
                    })
            })
        }
        else if (indicator.flag == "U") {
            pool.query("UPDATE mt_rectifier_indicator " +
                " SET  channel = $3, name = $4, color = $5, event_type = $6, last_update = now(), action = 'U'" +
                " WHERE id = $1 AND serial_number = $2 ;", [indicator.id, sn, indicator.channel, indicator.name, indicator.color, indicator.event], (error, results) => {

                    if (error) {
                        result(error, null);
                    }

                })
        }
        else if (indicator.flag == "D") {

            pool.query("UPDATE mt_rectifier_indicator " +
                " SET last_update = now(), flag = 'N', action = 'D'" +
                " WHERE id = $1 ;", [indicator.id], (error, results) => {

                    if (error) {
                        result(error, null);
                    }

                })
        }


    });

    pool.query("COMMIT;");

    result(null, "success");

}

const updateRectifierIndicator = (request, result) => {
    const { id, serialNumber, channel, name, color, eventType } = request.body;

    pool.query("UPDATE mt_rectifier_indicator " +
        " SET serial_number = $2, channel = $3, name = $4, color = $5, event_type = $6, last_update = now(), action = 'U'" +
        " WHERE id = $1 ;", [id, serialNumber, channel, name, color, eventType], (error, results) => {
            if (error) {
                result(error, null);
            }
            result(null, "success");
        })
}

const deleteRectifierIndicator = (request, result) => {
    const id = request.body.id;

    pool.query("UPDATE mt_rectifier_indicator " +
        " SET last_update = now(), flag = 'N', action = 'D'" +
        " WHERE id = $1 ;", [id], (error, results) => {
            if (error) {
                result(error, null);
            }
            result(null, "success");
        })
}

module.exports = { getRectifierIndicator, getRectifierIndicatorById, getRectifierIndicatorBySN, getRectifierIndicatorBySnAndChannel, getRectifierIndicatorBySnAndEvent, insertRectifierIndicator, updateRectifierIndicator, deleteRectifierIndicator };