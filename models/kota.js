const { pool } = require("../config/database.js");


const getKota = (result) => {
    pool.query("SELECT * FROM im_kota WHERE flag = 'Y' ORDER BY kota_id ASC", (error, results) => {
        if (error) {
            result(error,null);
        }
        result(null, results.rows);
    })
}

const getKotaById = (request, result) => {
    const id = request.params.id;

    pool.query("SELECT * FROM im_kota WHERE flag = 'Y' AND kota_id = $1", [id], (error, results) => {
        if (error) {
            result(error, null);
        }
        result(null, results.rows);
    })
}

const getKotaByProvinsi = (request, result) => {
    const id = request.params.id;

    pool.query("SELECT * FROM im_kota WHERE flag = 'Y' AND provinsi_id = $1", [id], (error, results) => {
        if (error) {
            result(error, null);
        }
        result(null, results.rows);
    })
}
module.exports = { getKota, getKotaById, getKotaByProvinsi };