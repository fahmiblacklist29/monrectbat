const express = require("express");
const bodyParser = require("body-parser");
const fileUpload = require("express-fileupload");
const morgan = require("morgan");
const cookieParser = require("cookie-parser");
const http = require("http");
const url = require("url");
const fs = require('fs');
const path = require("path");
const buffer = require("buffer");
const ip = require("ip");
const geoip = require('geoip-lite');

const { secret, appname } = require("../config/config");
const { getUsers, getUserById, createUser, updateUser, deleteUser } = require("../models/users");
const { getRectifier, getRectifierById, updateRectiName, getRectifierBySN, getRectifierByProvinsi, getRectifierByKota, insertRectifier, updateRectifier, deleteRectifier, getRectifierList, getRectifierWithLimit } = require("../models/rectifier");
const { getRectifierthresholdBySN, updateRoomTemp, updatePanelTemp, updateThreshInput, updateThreshLoad, updateThreshBatt, insertRectifierThreshold } = require("../models/rectifier-threshold");
const { getRectifierIndicatorBySN, insertRectifierIndicator } = require("../models/rectifier-indicator");
const { getFilteredLog } = require("../models/rectifier-log");
const { getEventLog, getEventLogByDate } = require("../models/event-log");

const { getDevice, getDeviceById, insertDevice, updateDevice, deleteDevice } = require("../utils/device");
const { getBattery, getBatteryById, insertBattery } = require("../utils/battery");
const { getProvinsi, getProvinsiById } = require("../models/provinsi");
const { getKota, getKotaById, getKotaByProvinsi } = require("../models/kota");
const { getKecamatan, getKecamatanById, getKecamatanByKota } = require("../models/kecamatan");
const { getDesa, getDesaByKecamatan, getDesaById } = require("../models/desa");
const { getMap, generatePin } = require("../utils/maps");
const { loginUser, validateToken, logoutUser } = require("../utils/login");
const { getWilayah, getWilayahById, insertWilayah, updateWilayah, deleteWilayah } = require("../models/wilayah");

const { result } = require("lodash");

const app = express();
const router = express.Router();
router.use(cookieParser());
router.use(bodyParser.json());
router.use(bodyParser.urlencoded({ extended: false }));
router.use(morgan('dev'));
router.use(fileUpload({ createParentPath: true }));

app.use(express.static('public'));



router.get('/', (req, res) => {
    res.render('notfound404');
})

router.get(appname + '/loginUser', (req, res) => {
    console.log(appname);
    res.render('login', { appname });
})
router.post(appname + '/login', loginUser)
router.get(appname + '/logout', logoutUser)


router.get(appname, (req, res) => {
    res.redirect(appname + "/dashboard");
})

router.get(appname + '/dashboard', validateToken, (req, res) => {
    let cookie = ('Cookies: ', req.cookies);
    var geo = [req.cookies.lat, req.cookies.long];

    getRectifier((err, results) => {

        if (err) {
            res.send(err)
        } else {
            res.cookie("rectifierList", results);
            res.render('dashboard', { appname, rectifierList: results, data: cookie, loc: geo, nav: "das" })
        }
    })
})

router.get(appname + '/location', validateToken, (req, res) => {
    let rectifierList = req.cookies.rectifierList;
    let params = url.parse(req.url, true).query;
    res.render('deviceDetail', { params, appname, rectifierList, nav: "loc" });
})

router.get(appname + '/location/:id', validateToken, (req, res) => {
    let rectifierList = req.cookies.rectifierList;
    let params = url.parse(req.url, true).query;

    getRectifierById(req, (err, results) => {
        if (!err) {
            devInfo = results;
            res.render('deviceDetail', { params, appname, rectifierList, devInfo: results[0], nav: "loc" });
        }
    });

})

router.post(appname + '/location/:id', validateToken, (req, res) => {
    let rectifierList = req.cookies.rectifierList;
    let params = url.parse(req.url, true).query;

    getRectifierById(req, (err, results) => {
        if (!err) {
            res.send(results);
        }
    });

})

router.get(appname + '/rectifierList', validateToken, (req, res) => {
    getRectifier((err, results) => {
        if (!err) {
            res.cookie("rectifierList", results);
            res.send(results);
        }
    });

})

router.get(appname + '/rectifierTop5List', validateToken, (req, res) => {
    getRectifierWithLimit('', (err, results) => {
        if (!err) {
            res.send(results);
        }
    });
})

router.get(appname + '/rectifierTop5List/:name', validateToken, (req, res) => {
    let name = req.params.name;
    getRectifierWithLimit(name, (err, results) => {
        if (!err) {
            res.send(results);
        }
    });
})

// ------ (start) Master Rectifier ------
router.get(appname + '/rectifier', validateToken, (req, res) => {
    let rectifierList = req.cookies.rectifierList;
    let params = url.parse(req.url, true).query;
    res.render('deviceDetail', { params, appname, rectifierList });
})

router.post(appname + '/rectifier/updRectiName', validateToken, (req, res) => {

    if (req.body && typeof req.body.id !== 'undefined' && typeof req.body.name !== 'undefined') {

        updateRectiName(req, (err, results) => {
            if (err) {
                res.send(err)
            } else {
                res.send(results);
                //res.json({msg : 'success', data : results });
            }
        });
    }
    else { res.send("No Data will be add !!!"); }
})

router.get(appname + '/rectifier/getTreshold/:sn', validateToken, (req, res) => {

    if (typeof req.params.sn !== 'undefined') {

        getRectifierthresholdBySN(req, (err, results) => {
            if (err) {
                res.send(err)
            } else {
                res.send(results);
                //res.json({msg : 'success', data : results });
            }
        });
    }
    else { res.send("No Data will be add !!!"); }
})


router.get(appname + '/rectifier/getIndicator/:sn', validateToken, (req, res) => {

    if (typeof req.params.sn !== 'undefined') {

        getRectifierIndicatorBySN(req, (err, results) => {
            if (err) {
                res.send(err)
            } else {
                res.send(results);
                //res.json({msg : 'success', data : results });
            }
        });
    }
    else { res.send("No Data will be add !!!"); }
})

router.post(appname + '/rectifier/getLog/', validateToken, (req, res) => {

    if (req.body && typeof req.body.sn !== 'undefined') {

        getFilteredLog(req, (err, results) => {
            if (err) {
                res.send(err)
            } else {
                res.send(results);
                //res.json({msg : 'success', data : results });
            }
        });
    }
    else { res.send("No Data will be add !!!"); }
})

router.post(appname + '/rectifier/updRoomTemp', validateToken, (req, res) => {
    if (req.body !== 'undefined' && typeof req.body.sn !== 'undefined') {
        updateRoomTemp(req, (err, results) => {
            if (err) {
                res.send(err)
            } else {
                res.send(results);
                //res.json({msg : 'success', data : results });
            }
        });
    }
    else { res.send("No Data will be add !!!"); }
})

router.post(appname + '/rectifier/updPanelTemp', validateToken, (req, res) => {
    if (req.body !== 'undefined' && typeof req.body.sn !== 'undefined') {
        updatePanelTemp(req, (err, results) => {
            if (err) {
                res.send(err)
            } else {
                res.send(results);
                //res.json({msg : 'success', data : results });
            }
        });
    }
    else { res.send("No Data will be add !!!"); }
})


router.post(appname + '/rectifier/updInputSett', validateToken, (req, res) => {
    if (req.body !== 'undefined' && typeof req.body.sn !== 'undefined') {
        updateThreshInput(req, (err, results) => {
            if (err) {
                res.send(err)
            } else {
                res.send(results);
                //res.json({msg : 'success', data : results });
            }
        });
    }
    else { res.send("No Data will be add !!!"); }
})

router.post(appname + '/rectifier/updLoadSett', validateToken, (req, res) => {
    if (req.body !== 'undefined' && typeof req.body.sn !== 'undefined') {
        updateThreshLoad(req, (err, results) => {
            if (err) {
                res.send(err)
            } else {
                res.send(results);
                //res.json({msg : 'success', data : results });
            }
        });
    }
    else { res.send("No Data will be add !!!"); }
})

router.post(appname + '/rectifier/updBattSett', validateToken, (req, res) => {
    if (req.body !== 'undefined' && typeof req.body.sn !== 'undefined') {
        updateThreshBatt(req, (err, results) => {
            if (err) {
                res.send(err)
            } else {
                res.send(results);
                //res.json({msg : 'success', data : results });
            }
        });
    }
    else { res.send("No Data will be add !!!"); }
})

router.post(appname + '/rectifier/updIndicator', validateToken, (req, res) => {
    if (req.body !== 'undefined' && typeof req.body.sn !== 'undefined') {

        insertRectifierIndicator(req, (err, results) => {
            if (err) {
                res.send(err)
            } else {
                res.send(results);
            }
        });
    }
    else { res.send("No Data will be add !!!"); }
})


// ------ (start) Master User ------
router.get(appname + '/users', validateToken, (req, res) => {
    let rectifierList = req.cookies.rectifierList;
    // console.log(rectifierList)
    getUsers((err, results) => {
        if (err) {
            res.send(err)
        } else {
            res.render('users', { jsonresult: results, appname, rectifierList, nav: "mas" });
        }
    })
})

router.get(appname + '/users/getAllUsers', validateToken, (req, res) => {
    getUsers((err, results) => {
        if (err) {
            res.send(err)
        } else {
            res.send(results);
        }
    })
})

router.get(appname + '/users/:id', validateToken, (req, res) => {
    getUserById(req, (err, results) => {
        if (err) {
            res.send(err)
        } else {
            res.send(results);
        }
    })
})

/** For creating master user */
router.post(appname + '/users/userAdd', validateToken, (req, res) => {

    if (req.body && typeof req.body.username !== 'undefined' && typeof req.body.password !== 'undefined') {

        createUser(req, (err, results) => {
            if (err) {
                res.send(err)
            } else {
                res.send(results);
                //res.json({msg : 'success', data : results });
            }
        });
    }
    else { res.send("No Data will be add !!!"); }
})

/** For updating master user */
router.post(appname + '/users/userUpd', validateToken, (req, res) => {

    if (req.body && typeof req.body.id !== 'undefined' && typeof req.body.username !== 'undefined' && typeof req.body.password !== 'undefined') {

        updateUser(req, (err, results) => {
            if (err) {
                res.send(err)
            } else {
                res.send(results);
                //res.json({msg : 'success', data : results });
            }
        });
    }
    else { res.send("No Data will be add !!!"); }
})

/** For deleting master user actualy just update flag*/
router.post(appname + '/users/userDel', validateToken, (req, res) => {

    if (req.body && typeof req.body.id !== 'undefined') {
        deleteUser(req, (err, results) => {
            if (err) {
                res.send(err)
            } else {
                res.send(results);
            }
        })
    }
})

router.post(appname + '/users', validateToken, (req, res) => {
    createUser(req, (err, results) => {
        if (err) {
            res.send(err)
        } else {
            res.send(results);
        }
    })
})

router.put(appname + '/users/:id', validateToken, (req, res) => {
    updateUser(req, (err, results) => {
        if (err) {
            res.send(err)
        } else {
            res.send(results);
        }
    })
})

router.delete(appname + '/users/:id', validateToken, (req, res) => {
    deleteUser(req, (err, results) => {
        if (err) {
            res.send(err)
        } else {
            res.send(results);
        }
    })
})

// ------ (start) Master Device ------
router.get(appname + '/device', validateToken, (req, res) => {
    let rectifierList = req.cookies.rectifierList;
    getDevice((err, results) => {
        if (err) {
            res.send(err)
        } else {
            res.render('device', { jsonresult: results, appname, rectifierList, nav: "mas" });
        }
    })
})

router.get(appname + '/device/getAll', validateToken, (req, res) => {
    getDevice((err, results) => {
        if (err) {
            res.send(err)
        } else {
            res.send(results);
        }
    });
})

router.get(appname + '/device/:id', validateToken, (req, res) => {
    getDeviceById(req, (err, results) => {
        if (err) {
            res.send(err)
        } else {
            // console.log(results)
            res.send(results);
        }
    })
})

/** For creating master device */
router.post(appname + '/device/deviceAdd', validateToken, (req, res) => {
    insertDevice(req, (err, results) => {
        if (err) {
            res.send(err)
        } else {
            res.send(results);
            //res.json({msg : 'success', data : results });
        }
    });
})

/** For updating master device */
router.post(appname + '/device/deviceUpd', validateToken, (req, res) => {
    updateDevice(req, (err, results) => {
        if (err) {
            res.send(err)
        } else {
            res.send(results);
            //res.json({msg : 'success', data : results });
        }
    });
})

/** For deleting master device actualy just update flag*/
router.post(appname + '/device/deviceDel', validateToken, (req, res) => {
    if (req.body && typeof req.body.id !== 'undefined') {
        deleteDevice(req, (err, results) => {
            if (err) {
                res.send(err)
            } else {
                res.send(results);
            }
        })
    }
})

// ------ (start) Master Battery ------
router.get(appname + '/battery', validateToken, (req, res) => {
    //let rectifierList = req.cookies.rectifierList;
    getBattery((err, results) => {
        if (err) {
            res.send(err)
        } else {
            res.render('battery', { jsonresult: results, appname, nav: "mas" });
        }
    })
})

router.get(appname + '/battery/batteryAdd', validateToken, (req, res) => {
    //let rectifierList = req.cookies.rectifierList;
    insertBattery(req, (err, results) => {
        if (err) {
            res.send(err)
        } else {
            res.send(results);
            //res.json({msg : 'success', data : results });
        }
    });
})


// ------ (start) Master Wilayah ------
router.get(appname + '/wilayah', validateToken, (req, res) => {
    let rectifierList = req.cookies.rectifierList;
    getWilayah((err, results) => {
        if (err) {
            res.send(err)
        } else {
            res.render('wilayah', { jsonresult: results, appname, rectifierList, nav: "mas" });
        }
    })
})

router.get(appname + '/wilayah/getAllWilayah', validateToken, (req, res) => {
    getWilayah((err, results) => {
        if (err) {
            res.send(err)
        } else {
            res.send(results);
        }
    });
})

router.get(appname + '/wilayah/:id', validateToken, (req, res) => {
    getWilayahById(req, (err, results) => {
        if (err) {
            res.send(err)
        } else {
            // console.log(results)
            res.send(results);
        }
    })
})

router.get(appname + '/wilayah/getByArea/:id', validateToken, (req, res) => {
    getWilayahById(req, (err, results) => {
        if (err) {
            res.send(err)
        } else {
            // console.log(results)
            res.send(results);
        }
    })
})

/** For creating master wilayah */
router.post(appname + '/wilayah/wilayahAdd', validateToken, (req, res) => {
    // console.log(req)
    insertWilayah(req, (err, results) => {
        if (err) {
            res.send(err)
        } else {
            res.send(results);
            //res.json({msg : 'success', data : results });
        }
    });
})

/** For updating master wilayah */
router.post(appname + '/wilayah/wilayahUpd', validateToken, (req, res) => {

    updateWilayah(req, (err, results) => {
        if (err) {
            res.send(err)
        } else {
            res.send(results);
            //res.json({msg : 'success', data : results });
        }
    });
})

/** For deleting master wilayah actualy just update flag*/
router.post(appname + '/wilayah/wilayahDel', validateToken, (req, res) => {
    if (req.body && typeof req.body.id !== 'undefined') {
        deleteWilayah(req, (err, results) => {
            if (err) {
                res.send(err)
            } else {
                res.send(results);
            }
        })
    }
})

// ------ (start) Report ------
router.get(appname + '/report', validateToken, (req, res) => {
    let rectifierList = req.cookies.rectifierList;
    // console.log(rectifierList)
    getEventLog((err, results) => {
        if (err) {
            res.send(err)
        } else {
            res.render('report', { jsonresult: results, appname, rectifierList, nav: "rep" });
        }
    })
})

router.get(appname + '/report/getAll', validateToken, (req, res) => {
    let rectifierList = req.cookies.rectifierList;
    getEventLog((err, results) => {
        if (err) {
            res.send(err)
        } else {
            res.render('report', { jsonresult: results, appname, rectifierList, nav: "rep" });
        }
    })
})

router.post(appname + '/report/getByDate', validateToken, (req, res) => {
    getEventLogByDate(req, (err, results) => {
        if (err) {
            res.send(err)
        } else {
            res.send(results);
        }
    })
})

// ------ (start) Provinsi, Kota, Kecamatan, Desa ------
router.get(appname + '/provinsi/getAll', validateToken, (req, res) => {
    getProvinsi((err, results) => {
        if (err) {
            res.send(err)
        } else {
            res.send(results);
        }
    });
})

router.get(appname + '/provinsi/:id', validateToken, (req, res) => {
    getProvinsiById(req, (err, results) => {
        if (err) {
            res.send(err)
        } else {
            res.send(results);
        }
    });
})

router.get(appname + '/kota/getAll', validateToken, (req, res) => {
    getKota((err, results) => {
        if (err) {
            res.send(err)
        } else {
            res.send(results);
        }
    });
})

router.get(appname + '/kota/:id', validateToken, (req, res) => {
    getKotaById(req, (err, results) => {
        if (err) {
            res.send(err)
        } else {
            res.send(results);
        }
    });
})

router.get(appname + '/kota/getByProvinsi/:id', validateToken, (req, res) => {
    getKotaByProvinsi(req, (err, results) => {
        if (err) {
            res.send(err)
        } else {
            res.send(results);
        }
    });
})

router.get(appname + '/kecamatan/getAll', validateToken, (req, res) => {
    getKecamatan((err, results) => {
        if (err) {
            res.send(err)
        } else {
            res.send(results);
        }
    });
})

router.get(appname + '/kecamatan/:id', validateToken, (req, res) => {
    getKecamatanById(req, (err, results) => {
        if (err) {
            res.send(err)
        } else {
            res.send(results);
        }
    });
})

router.get(appname + '/kecamatan/getByKota/:id', validateToken, (req, res) => {
    getKecamatanByKota(req, (err, results) => {
        if (err) {
            res.send(err)
        } else {
            res.send(results);
        }
    });
})

router.get(appname + '/desa/getAll', validateToken, (req, res) => {
    getDesa((err, results) => {
        if (err) {
            res.send(err)
        } else {
            res.send(results);
        }
    });
})

router.get(appname + '/desa/:id', validateToken, (req, res) => {
    getDesaById(req, (err, results) => {
        if (err) {
            res.send(err)
        } else {
            res.send(results);
        }
    });
})

router.get(appname + '/desa/getByKecamatan/:id', validateToken, (req, res) => {
    getDesaByKecamatan(req, (err, results) => {
        if (err) {
            res.send(err)
        } else {
            res.send(results);
        }
    });
})

module.exports = router;