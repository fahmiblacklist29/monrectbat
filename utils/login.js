const { pool } = require("../config/database.js");
const { secret, appname } = require("../config/config.js")
const md5 = require("md5");

const loginDb = (request, result) => {
    //console.log(request.body);
    const username = request.body.username;
    const password = md5(request.body.password);


    pool.query("SELECT a.*, b.wilayah_name, b.longitude, b.latitude FROM im_user a LEFT JOIN mt_wilayah b ON b.id = a.area_id" +
        " WHERE username = $1 AND password = $2", [username, password], (error, results) => {
            if (error) {
                result(error, null);
            } else {
                result(null, results.rows);
            }
        })
}

const logoutUser = (request, response) => {
    response.clearCookie("token");
    response.redirect(appname+"/loginUser");
}

const loginUser = (req, res) => {
    //console.log("Masuk Login")
    var jwt = req.app.get('jwt')
    loginDb(req, (err, results) => {
        if (err) {
            res.send(err)
        } else {
            try {
                // console.log(results)
                if (results.length > 0) {

                    var token = jwt.sign({ results }, secret, {
                        expiresIn: "24h"
                    });

                    // set response
                    res.json({
                        status: 1,
                        data: results,
                        token: token
                    })
                } else {
                    res.json({ 'msg': 'Eror , Check your username and password', 'status': 0 })
                }
            } catch (err) {
                console.log(err)
            }

        }
    })
}

const validateToken = (req, res, next) => {

    let jwt = req.app.get('jwt')

    //console.log('Cookies: ', req.cookies.token)
    let token = req.headers['Authorization'] || ('Cookies: ', req.cookies.token)
    //console.log(token)
    if (token) {
        jwt.verify(token, secret, function (err, decoded) {
            if (err)
                res.status(403).redirect(appname+'/loginUser')
                //return res.json({ status: 0, message: 'problem token' })
            else {
                req.decoded = decoded
                next()
            }
        })
    } else {
        return res.status(403).redirect(appname+'/loginUser')
    }
}


module.exports = { loginUser, validateToken, logoutUser }