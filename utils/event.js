const { pool } = require("../config/database.js");
const { getDevice } = require("./device");
const { getRectifierMonitoringBySN } = require("../models/rectifier-monitoring");
const { getRectifierIndicatorBySnAndChannel } = require("../models/rectifier-indicator");
const { Telegram, Telegraf } = require("telegraf");
const { botToken } = require("../config/config.js");
const { request } = require("express");
const { getUserTelegram } = require("../models/user-telegram.js");

const bot = new Telegraf(botToken);

const checkForNotif = (result) => {
	// console.log("====== INTERVAL ======")
	getDevice((err, results) => {

		if(err){return}
;		results.forEach(rectifier => {

			pool.query("SELECT * FROM mt_rectifier_monitoring WHERE flag = 'Y'  AND last_update < (now() - interval '2 minute') AND serial_number = $1", [rectifier.serial_number], (error, results) => {
				// console.log(results.rowCount + " ==> " + rectifier.serial_number)
				if (error) {
					console.log(error)
					return
					// result(error, null);
				}
				//--- Disconnect ---
				if (results.rowCount > 0) {
					pool.query("SELECT * FROM it_event_log WHERE serial_number = $1 AND status ILIKE 'Disconnected' AND flag_notif = 'Y'", [rectifier.serial_number], (error, results) => {
						if (error != null) {
							result(error, null);
						}
						if (results.rowCount == 0) {
							var data = {
								"serialNumber": rectifier.serial_number,
								"equipment": rectifier.rectifier_name,
								"status": "Disconnected",
								"eventType": "disconnect",
								"sensor": null,
								"value": null,
								"location": rectifier.kota_name,
								"flagNotif": "Y"
							};

							insertNotif(data, (errInsert, resInsert) => { });
							sendNotifTelegram(data, (errTele, resTele) => { });
						}
					})
					return;

				} else {
					// --- Connect ---
					pool.query("SELECT * FROM it_event_log WHERE serial_number = $1 AND status ILIKE 'Disconnected' AND flag_notif = 'Y'", [rectifier.serial_number], (error, results) => {
						if (error) {
							result(error, null);
						}
						if (results.rowCount > 0) {
							var data = {
								"serialNumber": rectifier.serial_number,
								"equipment": rectifier.rectifier_name,
								"status": "Connected",
								"eventType": "disconnect",
								"sensor": null,
								"value": null,
								"location": rectifier.kota_name,
								"flagNotif": "N"
							};

							insertNotif(data, (errInsert, resInsert) => { });

							var dataRecti = {
								"body": {
									"serialNumber": rectifier.serial_number,
									"status": "Disconnected",
									"eventType": "disconnect",
									"sensor": null
								}
							}
							// console.log(dataRecti.body);
							updateNotif(dataRecti, (errUpdate, resUpdate) => { });

							sendNotifTelegram(data, (errTele, resTele) => { });
						}
					})
				}
			})

			switchStateThreshold(rectifier, (err, resThresh) => {
				if (err == null) {
					if (resThresh.dif_volt_input == true) {
						var status;
						if (resThresh.con_volt_input == true) {
							status = 'Normal';
							flagNotif = 'N';

							var dataRecti = {
								"serialNumber": rectifier.serial_number,
								"status": "Out of Limit",
								"eventType": "alarm",
								"sensor": "Voltage LN"
							}
							updateNotif(dataRecti, (errUpdate, resUpdate) => { });

						} else {
							status = 'Out of Limit';
							flagNotif = 'Y';
						}

						var data = {
							"serialNumber": rectifier.serial_number,
							"equipment": rectifier.rectifier_name,
							"status": status,
							"eventType": "alarm",
							"sensor": "Voltage LN",
							"value": resThresh.val_volt_input,
							"location": rectifier.kota_name,
							"flagNotif": flagNotif
						};

						insertNotif(data, (errInsert, resInsert) => { });
						sendNotifTelegram(data, (errTele, resTele) => { });
					}

					if (resThresh.dif_current_input == true) {
						var status;
						if (resThresh.con_current_input == true) {
							status = 'Normal';
							flagNotif = 'N';

							var dataRecti = {
								"serialNumber": rectifier.serial_number,
								"status": "Out of Limit",
								"eventType": "alarm",
								"sensor": "Current L"
							}
							updateNotif(dataRecti, (errUpdate, resUpdate) => { });
						} else {
							status = 'Out of Limit';
							flagNotif = 'Y';
						}

						var data = {
							"serialNumber": rectifier.serial_number,
							"equipment": rectifier.rectifier_name,
							"status": status,
							"eventType": "alarm",
							"sensor": "Current L",
							"value": resThresh.val_current_input,
							"location": rectifier.kota_name,
							"flagNotif": flagNotif
						};

						insertNotif(data, (errInsert, resInsert) => { });
						sendNotifTelegram(data, (errTele, resTele) => { });
					}

					if (resThresh.dif_volt_load == true) {
						var status;
						if (resThresh.con_volt_load == true) {
							status = 'Normal';
							flagNotif = 'N';

							var dataRecti = {
								"serialNumber": rectifier.serial_number,
								"status": "Out of Limit",
								"eventType": "alarm",
								"sensor": "Voltage Load"
							}
							updateNotif(dataRecti, (errUpdate, resUpdate) => { });
						} else {
							status = 'Out of Limit';
							flagNotif = 'Y';
						}

						var data = {
							"serialNumber": rectifier.serial_number,
							"equipment": rectifier.rectifier_name,
							"status": status,
							"eventType": "alarm",
							"sensor": "Voltage Load",
							"value": resThresh.val_volt_load,
							"location": rectifier.kota_name,
							"flagNotif": flagNotif
						};

						insertNotif(data, (errInsert, resInsert) => { });
						sendNotifTelegram(data, (errTele, resTele) => { });
					}

					if (resThresh.dif_current_load == true) {
						var status;
						if (resThresh.con_current_load == true) {
							status = 'Normal';
							flagNotif = 'N';

							var dataRecti = {
								"serialNumber": rectifier.serial_number,
								"status": "Out of Limit",
								"eventType": "alarm",
								"sensor": "Current Load"
							}
							updateNotif(dataRecti, (errUpdate, resUpdate) => { });
						} else {
							status = 'Out of Limit';
							flagNotif = 'Y';
						}

						var data = {
							"serialNumber": rectifier.serial_number,
							"equipment": rectifier.rectifier_name,
							"status": status,
							"eventType": "alarm",
							"sensor": "Current Load",
							"value": resThresh.val_current_load,
							"location": rectifier.kota_name,
							"flagNotif": flagNotif
						};

						insertNotif(data, (errInsert, resInsert) => { });
						sendNotifTelegram(data, (errTele, resTele) => { });
					}

					if (resThresh.dif_volt_battery == true) {
						var status;
						if (resThresh.con_volt_battery == true) {
							status = 'Normal';
							flagNotif = 'N';

							var dataRecti = {
								"serialNumber": rectifier.serial_number,
								"status": "Out of Limit",
								"eventType": "alarm",
								"sensor": "Voltage Battery"
							}
							updateNotif(dataRecti, (errUpdate, resUpdate) => { });
						} else {
							status = 'Out of Limit';
							flagNotif = 'Y';
						}

						var data = {
							"serialNumber": rectifier.serial_number,
							"equipment": rectifier.rectifier_name,
							"status": status,
							"eventType": "alarm",
							"sensor": "Voltage Battery",
							"value": resThresh.val_volt_battery,
							"location": rectifier.kota_name,
							"flagNotif": flagNotif
						};

						insertNotif(data, (errInsert, resInsert) => { });
						sendNotifTelegram(data, (errTele, resTele) => { });
					}

					if (resThresh.dif_current_battery == true) {
						var status;
						if (resThresh.con_current_battery == true) {
							status = 'Normal';
							flagNotif = 'N';

							var dataRecti = {
								"serialNumber": rectifier.serial_number,
								"status": "Out of Limit",
								"eventType": "alarm",
								"sensor": "Current Battery"
							}
							updateNotif(dataRecti, (errUpdate, resUpdate) => { });
						} else {
							status = 'Out of Limit';
							flagNotif = 'Y';
						}

						var data = {
							"serialNumber": rectifier.serial_number,
							"equipment": rectifier.rectifier_name,
							"status": status,
							"eventType": "alarm",
							"sensor": "Current Battery",
							"value": resThresh.val_current_battery,
							"location": rectifier.kota_name,
							"flagNotif": flagNotif
						};

						insertNotif(data, (errInsert, resInsert) => { });
						sendNotifTelegram(data, (errTele, resTele) => { });
					}

				}
			});

			switchStateIndicator(rectifier, (err, resIndicator) => {
				if (resIndicator.dif_channel_1 == true) {
					var dataRecti = { "params": { "id": rectifier.serial_number, "ch": "CH-1" } };
					getRectifierIndicatorBySnAndChannel(dataRecti, (err, resMasIndic) => {
						if (resMasIndic.length() > 0) {
							var stateIndicator;
							if (resIndicator.val_channel_1 == true) {
								stateIndicator = "Indicator ON";
							} else {
								stateIndicator = "Indicator OFF";
							}
							var data = {
								"serialNumber": rectifier.serial_number,
								"equipment": rectifier.rectifier_name,
								"status": stateIndicator,
								"eventType": "alarm",
								"sensor": resMasIndic.name,
								"value": null,
								"location": rectifier.kota_name,
								"flagNotif": "N"
							};

							insertNotif(data, (errInsert, resInsert) => { });

							if (resMasIndic[0].eventType.toLowerCase() == "alarm") {
								sendNotifTelegram(data, (errTele, resTele) => { });
							}
						}
					})
				}
				if (resIndicator.dif_channel_2 == true) {
					var dataRecti = { "params": { "id": rectifier.serial_number, "ch": "CH-2" } };
					getRectifierIndicatorBySnAndChannel(dataRecti, (err, resMasIndic) => {
						if (resMasIndic.length() > 0) {
							var stateIndicator;
							if (resIndicator.val_channel_2 == true) {
								stateIndicator = "Indicator ON";
							} else {
								stateIndicator = "Indicator OFF";
							}
							var data = {
								"serialNumber": rectifier.serial_number,
								"equipment": rectifier.rectifier_name,
								"status": stateIndicator,
								"eventType": "alarm",
								"sensor": resMasIndic.name,
								"value": null,
								"location": rectifier.kota_name,
								"flagNotif": "N"
							};

							insertNotif(data, (errInsert, resInsert) => { });

							if (resMasIndic[0].eventType.toLowerCase() == "alarm") {
								sendNotifTelegram(data, (errTele, resTele) => { });
							}
						}
					})
				}
				if (resIndicator.dif_channel_3 == true) {
					var dataRecti = { "params": { "id": rectifier.serial_number, "ch": "CH-3" } };
					getRectifierIndicatorBySnAndChannel(dataRecti, (err, resMasIndic) => {
						if (resMasIndic.length() > 0) {
							var stateIndicator;
							if (resIndicator.val_channel_3 == true) {
								stateIndicator = "Indicator ON";
							} else {
								stateIndicator = "Indicator OFF";
							}
							var data = {
								"serialNumber": rectifier.serial_number,
								"equipment": rectifier.rectifier_name,
								"status": stateIndicator,
								"eventType": "alarm",
								"sensor": resMasIndic.name,
								"value": null,
								"location": rectifier.kota_name,
								"flagNotif": "N"
							};

							insertNotif(data, (errInsert, resInsert) => { });

							if (resMasIndic[0].eventType.toLowerCase() == "alarm") {
								sendNotifTelegram(data, (errTele, resTele) => { });
							}
						}
					})
				}
				if (resIndicator.dif_channel_4 == true) {
					var dataRecti = { "params": { "id": rectifier.serial_number, "ch": "CH-4" } };
					getRectifierIndicatorBySnAndChannel(dataRecti, (err, resMasIndic) => {
						if (resMasIndic.length() > 0) {
							var stateIndicator;
							if (resIndicator.val_channel_4 == true) {
								stateIndicator = "Indicator ON";
							} else {
								stateIndicator = "Indicator OFF";
							}
							var data = {
								"serialNumber": rectifier.serial_number,
								"equipment": rectifier.rectifier_name,
								"status": stateIndicator,
								"eventType": "alarm",
								"sensor": resMasIndic.name,
								"value": null,
								"location": rectifier.kota_name,
								"flagNotif": "N"
							};

							insertNotif(data, (errInsert, resInsert) => { });

							if (resMasIndic[0].eventType.toLowerCase() == "alarm") {
								sendNotifTelegram(data, (errTele, resTele) => { });
							}
						}
					})
				}
				if (resIndicator.dif_channel_5 == true) {
					var dataRecti = { "params": { "id": rectifier.serial_number, "ch": "CH-5" } };
					getRectifierIndicatorBySnAndChannel(dataRecti, (err, resMasIndic) => {
						if (resMasIndic.length() > 0) {
							var stateIndicator;
							if (resIndicator.val_channel_5 == true) {
								stateIndicator = "Indicator ON";
							} else {
								stateIndicator = "Indicator OFF";
							}
							var data = {
								"serialNumber": rectifier.serial_number,
								"equipment": rectifier.rectifier_name,
								"status": stateIndicator,
								"eventType": "alarm",
								"sensor": resMasIndic.name,
								"value": null,
								"location": rectifier.kota_name,
								"flagNotif": "N"
							};

							insertNotif(data, (errInsert, resInsert) => { });

							if (resMasIndic[0].eventType.toLowerCase() == "alarm") {
								sendNotifTelegram(data, (errTele, resTele) => { });
							}
						}
					})
				}
				if (resIndicator.dif_channel_6 == true) {
					var dataRecti = { "params": { "id": rectifier.serial_number, "ch": "CH-6" } };
					getRectifierIndicatorBySnAndChannel(dataRecti, (err, resMasIndic) => {
						if (resMasIndic.length() > 0) {
							var stateIndicator;
							if (resIndicator.val_channel_6 == true) {
								stateIndicator = "Indicator ON";
							} else {
								stateIndicator = "Indicator OFF";
							}
							var data = {
								"serialNumber": rectifier.serial_number,
								"equipment": rectifier.rectifier_name,
								"status": stateIndicator,
								"eventType": "alarm",
								"sensor": resMasIndic.name,
								"value": null,
								"location": rectifier.kota_name,
								"flagNotif": "N"
							};

							insertNotif(data, (errInsert, resInsert) => { });

							if (resMasIndic[0].eventType.toLowerCase() == "alarm") {
								sendNotifTelegram(data, (errTele, resTele) => { });
							}
						}
					})
				}
				if (resIndicator.dif_channel_7 == true) {
					var dataRecti = { "params": { "id": rectifier.serial_number, "ch": "CH-7" } };
					getRectifierIndicatorBySnAndChannel(dataRecti, (err, resMasIndic) => {
						if (resMasIndic.length() > 0) {
							var stateIndicator;
							if (resIndicator.val_channel_7 == true) {
								stateIndicator = "Indicator ON";
							} else {
								stateIndicator = "Indicator OFF";
							}
							var data = {
								"serialNumber": rectifier.serial_number,
								"equipment": rectifier.rectifier_name,
								"status": stateIndicator,
								"eventType": "alarm",
								"sensor": resMasIndic.name,
								"value": null,
								"location": rectifier.kota_name,
								"flagNotif": "N"
							};

							insertNotif(data, (errInsert, resInsert) => { });

							if (resMasIndic[0].eventType.toLowerCase() == "alarm") {
								sendNotifTelegram(data, (errTele, resTele) => { });
							}
						}
					})
				}
				if (resIndicator.dif_channel_8 == true) {
					var dataRecti = { "params": { "id": rectifier.serial_number, "ch": "CH-8" } };
					getRectifierIndicatorBySnAndChannel(dataRecti, (err, resMasIndic) => {
						if (resMasIndic.length() > 0) {
							var stateIndicator;
							if (resIndicator.val_channel_8 == true) {
								stateIndicator = "Indicator ON";
							} else {
								stateIndicator = "Indicator OFF";
							}
							var data = {
								"serialNumber": rectifier.serial_number,
								"equipment": rectifier.rectifier_name,
								"status": stateIndicator,
								"eventType": "alarm",
								"sensor": resMasIndic.name,
								"value": null,
								"location": rectifier.kota_name,
								"flagNotif": "N"
							};

							insertNotif(data, (errInsert, resInsert) => { });

							if (resMasIndic[0].eventType.toLowerCase() == "alarm") {
								sendNotifTelegram(data, (errTele, resTele) => { });
							}
						}
					})
				}
				if (resIndicator.dif_channel_9 == true) {
					var dataRecti = { "params": { "id": rectifier.serial_number, "ch": "CH-9" } };
					getRectifierIndicatorBySnAndChannel(dataRecti, (err, resMasIndic) => {
						if (resMasIndic.length() > 0) {
							var stateIndicator;
							if (resIndicator.val_channel_9 == true) {
								stateIndicator = "Indicator ON";
							} else {
								stateIndicator = "Indicator OFF";
							}
							var data = {
								"serialNumber": rectifier.serial_number,
								"equipment": rectifier.rectifier_name,
								"status": stateIndicator,
								"eventType": "alarm",
								"sensor": resMasIndic.name,
								"value": null,
								"location": rectifier.kota_name,
								"flagNotif": "N"
							};

							insertNotif(data, (errInsert, resInsert) => { });

							if (resMasIndic[0].eventType.toLowerCase() == "alarm") {
								sendNotifTelegram(data, (errTele, resTele) => { });
							}
						}
					})
				}
				if (resIndicator.dif_channel_10 == true) {
					var dataRecti = { "params": { "id": rectifier.serial_number, "ch": "CH-10" } };
					getRectifierIndicatorBySnAndChannel(dataRecti, (err, resMasIndic) => {
						if (resMasIndic.length() > 0) {
							var stateIndicator;
							if (resIndicator.val_channel_10 == true) {
								stateIndicator = "Indicator ON";
							} else {
								stateIndicator = "Indicator OFF";
							}
							var data = {
								"serialNumber": rectifier.serial_number,
								"equipment": rectifier.rectifier_name,
								"status": stateIndicator,
								"eventType": "alarm",
								"sensor": resMasIndic.name,
								"value": null,
								"location": rectifier.kota_name,
								"flagNotif": "N"
							};

							insertNotif(data, (errInsert, resInsert) => { });

							if (resMasIndic[0].eventType.toLowerCase() == "alarm") {
								sendNotifTelegram(data, (errTele, resTele) => { });
							}
						}
					})
				}
			})
		});
	})
}

const switchStateThreshold = (request, result) => {
	pool.query("SELECT " +
		" (a.voltage_ln BETWEEN b.voltage_ln_min AND b.voltage_ln_max) != (c.voltage_ln BETWEEN b.voltage_ln_min AND b.voltage_ln_max) AS dif_volt_input, " +
		" (a.current_l BETWEEN b.current_l_min AND b.current_l_max) != (c.current_l BETWEEN b.current_l_min AND b.current_l_max) AS dif_current_input, " +
		" (a.voltage_load BETWEEN b.voltage_load_min AND b.voltage_load_max) != (c.voltage_load BETWEEN b.voltage_load_min AND b.voltage_load_max) AS dif_volt_load, " +
		" (a.current_load BETWEEN b.current_load_min AND b.current_load_max) != (c.current_load BETWEEN b.current_load_min AND b.current_load_max) AS dif_current_load, " +
		" (a.voltage_battery BETWEEN b.voltage_batt_min AND b.voltage_batt_max) != (c.voltage_battery BETWEEN b.voltage_batt_min AND b.voltage_batt_max) AS dif_volt_battery, " +
		" (a.current_battery BETWEEN b.current_batt_min AND b.current_batt_max) != (c.voltage_battery BETWEEN b.voltage_batt_min AND b.voltage_batt_max) AS dif_current_load, " +
		" (a.voltage_ln BETWEEN b.voltage_ln_min AND b.voltage_ln_max) AS con_volt_input, " +
		" (a.current_l BETWEEN b.current_l_min AND b.current_l_max) AS con_current_input, " +
		" (a.voltage_load BETWEEN b.voltage_load_min AND b.voltage_load_max) AS con_volt_load, " +
		" (a.current_load BETWEEN b.current_load_min AND b.current_load_max) AS con_current_load, " +
		" (a.voltage_battery BETWEEN b.voltage_batt_min AND b.voltage_batt_max) AS con_volt_battery, " +
		" (a.current_battery BETWEEN b.current_batt_min AND b.current_batt_max) AS con_current_load, " +
		" a.voltage_ln AS val_volt_input, " +
		" a.current_l AS val_current_input, " +
		" a.voltage_load AS val_volt_load, " +
		" a.current_load AS val_current_load, " +
		" a.voltage_battery AS val_volt_battery, " +
		" a.current_battery AS val_current_load " +
		" FROM mt_rectifier_monitoring a " +
		" LEFT JOIN mt_rectifier_threshold b ON b.serial_number = a.serial_number " +
		" LEFT JOIN ( " +
		"   SELECT * FROM it_rectifier_log " +
		"   WHERE serial_number = $1 " +
		"	AND created_date < ( " +
		"		SELECT created_date FROM it_rectifier_log " +
		"		WHERE serial_number = $1 " +
		"		ORDER BY created_date DESC " +
		"		LIMIT 1) " +
		"   ORDER BY created_date DESC " +
		"   LIMIT 1) c ON c.serial_number = a.serial_number " +
		" WHERE a.serial_number = $1;", [request.serial_number], (error, results) => {
			if (error) {
				console.log(error)
				result(error, null);
			}
			// console.log(results)
			if (results.rowCount > 0) {
				result(null, results.rows);
			} else {
				result(null, JSON.parse("[]"));
			}

		})
}

const switchStateIndicator = (request, result) => {
	pool.query("SELECT " +
		" 	a.channel_1 != b.channel_1 AS dif_channel_1, " +
		" 	a.channel_2 != b.channel_2 AS dif_channel_2, " +
		" 	a.channel_3 != b.channel_3 AS dif_channel_3, " +
		" 	a.channel_4 != b.channel_4 AS dif_channel_4, " +
		" 	a.channel_5 != b.channel_5 AS dif_channel_5, " +
		" 	a.channel_6 != b.channel_6 AS dif_channel_6, " +
		" 	a.channel_7 != b.channel_7 AS dif_channel_7, " +
		" 	a.channel_8 != b.channel_8 AS dif_channel_8, " +
		" 	a.channel_9 != b.channel_9 AS dif_channel_9, " +
		" 	a.channel_10 != b.channel_10 AS dif_channel_10, " +
		" 	a.channel_1 AS val_channel_1, " +
		" 	a.channel_2 AS val_channel_2, " +
		" 	a.channel_3 AS val_channel_3, " +
		" 	a.channel_4 AS val_channel_4, " +
		" 	a.channel_5 AS val_channel_5, " +
		" 	a.channel_6 AS val_channel_6, " +
		" 	a.channel_7 AS val_channel_7, " +
		" 	a.channel_8 AS val_channel_8, " +
		" 	a.channel_9 AS val_channel_9, " +
		" 	a.channel_10 AS val_channel_10 " +
		" FROM mt_rectifier_monitoring a  " +
		" LEFT JOIN (  " +
		" 	SELECT * FROM it_rectifier_log  " +
		" 	WHERE serial_number = $1  " +
		" 	AND created_date < (  " +
		" 		SELECT created_date FROM it_rectifier_log  " +
		" 		WHERE serial_number = $1  " +
		" 		ORDER BY created_date DESC  " +
		" 		LIMIT 1)  " +
		" 	ORDER BY created_date DESC  " +
		" 	LIMIT 1) b ON b.serial_number = a.serial_number " +
		" WHERE a.serial_number = $1;", [request.serial_number], (error, results) => {
			if (error) {
				result(error, null);
			}
			if (results.rowCount > 0) {
				result(null, results.rows);
			} else {
				result(null, JSON.parse("[]"));
			}

		})
}

const insertNotif = (data, result) => {
	const { serialNumber, equipment, status, eventType, sensor, value, location, flagNotif } = data;
	var seqId;

	pool.query("SELECT nextval('seq_event_log');", (error, results) => {
		if (error) {
			throw error;
		}
		seqId = results.rows[0].nextval;

		pool.query("INSERT INTO it_event_log( " +
			" id, serial_number, equipment, status, event_type, sensor, value, location, created_date, last_update, flag_notif) " +
			" VALUES ($1, $2, $3, $4, $5, $6, $7, $8, now(), now(), $9);", [seqId, serialNumber, equipment, status, eventType, sensor, value, location, flagNotif], (error, results) => {
				if (error) {
					result(error, null);
				}
				result(null, "success");
			})
	})

}

const updateNotif = (request, result) => {
	const { serialNumber, status, eventType, sensor } = request.body;

	if (eventType == 'alarm') {
		pool.query("UPDATE it_event_log " +
			" SET last_update = now(), flag_notif = 'N' " +
			" WHERE serial_number = $1 AND status = $2 AND event_type = $3 AND sensor ILIKE $4 AND flag_notif = 'Y' ;", [serialNumber, status, eventType, sensor], (error, results) => {
				if (error) {
					result(error, null);
				}
				result(null, "success");
			})
	} else {
		pool.query("UPDATE it_event_log " +
			" SET last_update = now(), flag_notif = 'N' " +
			" WHERE serial_number = $1 AND status = $2 AND event_type = $3 AND flag_notif = 'Y' ;", [serialNumber, status, eventType], (error, results) => {
				if (error) {
					result(error, null);
				}
				result(null, "success");
			})
	}

}

const sendNotifTelegram = (request, result) => {
	const { equipment, status, eventType, sensor, value, location } = request;

	var dateNow = new Date();

	var notifMessage = "[" + status.toUpperCase() + "]" +
		"\nDevice : " + equipment;
	if (eventType.toLowerCase() == "alarm") {
		if (value != null) {
			notifMessage += "\nSensor : " + sensor + "\nValue : " + value;
		} else {
			notifMessage += "\nIndicator : " + sensor;
		}
	}
	notifMessage += "\nLocation : " + location +
		"\nTime : " + dateNow.getFullYear() + "-" + ("0"+(dateNow.getMonth()+1)).slice(-2) + "-" + ("0" + dateNow.getDate()).slice(-2) + " " + ("0"+dateNow.getHours()).slice(-2) + ":" + ("0"+dateNow.getMinutes()).slice(-2);

	getUserTelegram((err, resTele) => {
		// console.log(resTele);
		if (err == null) {
			resTele.forEach(userTele => {
				bot.telegram.sendMessage(userTele.chat_id, notifMessage);
			});
		} else {
			console.log(err);
		}
	})
}

module.exports = { checkForNotif }