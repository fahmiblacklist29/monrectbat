const { pool } = require("../config/database.js");


const getDevice = (result) => {
    pool.query("SELECT a.rectifier_id, a.rectifier_name, a.lat, a.long, a.desa_id, a.kecamatan_id, a.kota_id, a.provinsi_id, a.mqtt_topic, a.description, " +
        " b.desa_name, c.kecamatan_name, d.kota_name, e.provinsi_name," +
        " f.*" +
        " FROM im_rectifier a" +
        " LEFT JOIN im_desa b ON b.desa_id = a.desa_id" +
        " LEFT JOIN im_kecamatan c ON c.kecamatan_id = a.kecamatan_id" +
        " LEFT JOIN im_kota d ON d.kota_id = a.kota_id" +
        " LEFT JOIN im_provinsi e ON e.provinsi_id = a.provinsi_id" +
        " LEFT JOIN  mt_rectifier_threshold f ON f.serial_number = a.serial_number" +
        " WHERE a.flag = 'Y' ORDER BY a.rectifier_id ASC", (error, results) => {
            if (error) {
                result(error, null);
            }
            if (results.rowCount > 0) {
                result(null,
                    results.rows.sort((a, b) => {
                        return a.kota_id.localeCompare(b.kota_id);
                    }));
            } else {
                result(null, JSON.parse("[]"));
            }

        })
}

const getDeviceById = (request, result) => {
    const id = request.params.id;

    pool.query("SELECT a.rectifier_id, a.rectifier_name, a.lat, a.long, a.desa_id, a.kecamatan_id, a.kota_id, a.provinsi_id, a.mqtt_topic, a.description, " +
        " b.desa_name, c.kecamatan_name, d.kota_name, e.provinsi_name," +
        " f.*" +
        " FROM im_rectifier a" +
        " LEFT JOIN im_desa b ON b.desa_id = a.desa_id" +
        " LEFT JOIN im_kecamatan c ON c.kecamatan_id = a.kecamatan_id" +
        " LEFT JOIN im_kota d ON d.kota_id = a.kota_id" +
        " LEFT JOIN im_provinsi e ON e.provinsi_id = a.provinsi_id" +
        " LEFT JOIN  mt_rectifier_threshold f ON f.serial_number = a.serial_number" +
        " WHERE a.flag = 'Y' AND a.rectifier_id = $1", [id], (error, results) => {
            if (error) {
                result(error, null);
            }
            result(null, results.rows);
        })
}

const getDeviceBySN = (request, result) => {
    const id = request.params.id;

    pool.query("SELECT a.rectifier_id, a.rectifier_name, a.lat, a.long, a.desa_id, a.kecamatan_id, a.kota_id, a.provinsi_id, a.mqtt_topic, a.description, " +
        " b.desa_name, c.kecamatan_name, d.kota_name, e.provinsi_name," +
        " f.*" +
        " FROM im_rectifier a" +
        " LEFT JOIN im_desa b ON b.desa_id = a.desa_id" +
        " LEFT JOIN im_kecamatan c ON c.kecamatan_id = a.kecamatan_id" +
        " LEFT JOIN im_kota d ON d.kota_id = a.kota_id" +
        " LEFT JOIN im_provinsi e ON e.provinsi_id = a.provinsi_id" +
        " LEFT JOIN  mt_rectifier_threshold f ON f.serial_number = a.serial_number" +
        " WHERE a.flag = 'Y' AND a.serial_number = $1", [id], (error, results) => {
            if (error) {
                result(error, null);
            }
            result(null, results.rows);
        })
}

const getDeviceByProvinsi = (request, result) => {
    const id = request.params.id;

    pool.query("SELECT a.rectifier_id, a.rectifier_name, a.lat, a.long, a.desa_id, a.kecamatan_id, a.kota_id, a.provinsi_id, a.mqtt_topic, a.description, " +
        " b.desa_name, c.kecamatan_name, d.kota_name, e.provinsi_name," +
        " f.*" +
        " FROM im_rectifier a" +
        " LEFT JOIN im_desa b ON b.desa_id = a.desa_id" +
        " LEFT JOIN im_kecamatan c ON c.kecamatan_id = a.kecamatan_id" +
        " LEFT JOIN im_kota d ON d.kota_id = a.kota_id" +
        " LEFT JOIN im_provinsi e ON e.provinsi_id = a.provinsi_id" +
        " LEFT JOIN  mt_rectifier_threshold f ON f.serial_number = a.serial_number" +
        " WHERE a.flag = 'Y' AND a.provinsi_id = $1", [id], (error, results) => {
            if (error) {
                result(error, null);
            }
            result(null, results.rows);
        })
}

const getDeviceByKota = (request, result) => {
    const id = request.params.id;

    pool.query("SELECT a.rectifier_id, a.rectifier_name, a.lat, a.long, a.desa_id, a.kecamatan_id, a.kota_id, a.provinsi_id, a.mqtt_topic, a.description, " +
        " b.desa_name, c.kecamatan_name, d.kota_name, e.provinsi_name," +
        " f.*" +
        " FROM im_rectifier a" +
        " LEFT JOIN im_desa b ON b.desa_id = a.desa_id" +
        " LEFT JOIN im_kecamatan c ON c.kecamatan_id = a.kecamatan_id" +
        " LEFT JOIN im_kota d ON d.kota_id = a.kota_id" +
        " LEFT JOIN im_provinsi e ON e.provinsi_id = a.provinsi_id" +
        " LEFT JOIN  mt_rectifier_threshold f ON f.serial_number = a.serial_number" +
        " WHERE a.flag = 'Y' AND a.kota_id = $1", [id], (error, results) => {
            if (error) {
                result(error, null);
            }
            result(null, results.rows);
        })
}

const insertDevice = (request, result) => {
    const { name, lat, lon, desa, kecamatan, kota, provinsi, serialNumber, description,
        voltLoadMin, voltLoadMax, curLoadMin, curLoadMax, voltBattMin, voltBattMax, curBattMin, curBattMax, voltLnMin, voltLnMax, curLMin, curLMax, roomTempMin, roomTempMax, panelTempMin, panelTempMax } = request.body;
    const byWho = request.cookies.username;


    (async () => {
        // note: we don't try/catch this because if connecting throws an exception
        // we don't need to dispose of the client (it will be undefined)
        const client = await pool.connect();
        try {
            await client.query('BEGIN');

            const mqttTopic = "monRectiData/" + serialNumber;

            const querySeqRectifier = "SELECT nextval('seq_rectifier');"
            const seqRecti = await client.query(querySeqRectifier);

            const querySeqThresh = "SELECT nextval('seq_rectifier_threshold');"
            const seqThresh = await client.query(querySeqThresh);

            const valueRectifier = [seqRecti.rows[0].nextval, name, lat, lon, desa, kecamatan, kota, provinsi, mqttTopic, serialNumber, description];
            const valueThreshold = [seqThresh.rows[0].nextval, serialNumber, voltLoadMin, voltLoadMax, curLoadMin, curLoadMax, voltBattMin, voltBattMax, curBattMin, curBattMax, voltLnMin, voltLnMax, curLMin, curLMax, roomTempMin, roomTempMax, panelTempMin, panelTempMax];

            const queryInsertRecti = " INSERT INTO im_rectifier(" +
                ' rectifier_id, rectifier_name, lat, "long", desa_id, kecamatan_id, kota_id, provinsi_id, mqtt_topic, serial_number, created_who, description, created_date, last_update_who, last_update, flag, action)' +
                " VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11,  $12, now(), $11, now(), 'Y', 'C');";
            const queryInsertThresh = " INSERT INTO mt_rectifier_threshold(" +
                " id, serial_number, voltage_load_min, voltage_load_max, current_load_min, current_load_max, voltage_batt_min, voltage_batt_max, current_batt_min, current_batt_max, voltage_ln_min, voltage_ln_max, current_l_min, current_l_max, room_temp_min, room_temp_max, panel_temp_min, panel_temp_max, created_date, last_update, flag, action) " +
                " VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, $13, $14, $15, $16, $17, $18, now(), now(), 'Y', 'C');";

            await client.query(queryInsertRecti, valueRectifier);
            await client.query(queryInsertThresh, valueThreshold);

            await client.query('COMMIT');
        } catch (e) {
            await client.query('ROLLBACK');
            result(e, null);
            throw e
        } finally {
            client.release()
        }
        result(null, "success");
    })().catch(e => console.error(e.stack));
}


const updateDevice = (request, result) => {
    const { id, name, lat, lon, desa, kecamatan, kota, provinsi, serialNumber, description,
        voltLoadMin, voltLoadMax, curLoadMin, curLoadMax, voltBattMin, voltBattMax, curBattMin, curBattMax, voltLnMin, voltLnMax, curLMin, curLMax, roomTempMin, roomTempMax, panelTempMin, panelTempMax } = request.body;
    const byWho = request.cookies.username;


    (async () => {
        // note: we don't try/catch this because if connecting throws an exception
        // we don't need to dispose of the client (it will be undefined)
        const client = await pool.connect();
        try {
            await client.query('BEGIN');

            const mqttTopic = "monRectiData/" + serialNumber;

            const valueRectifier = [id, name, serialNumber, lat, lon, desa, kecamatan, kota, provinsi, mqttTopic, byWho, description];
            const valueThreshold = [voltLoadMin, voltLoadMax, curLoadMin, curLoadMax, voltBattMin, voltBattMax, curBattMin, curBattMax, voltLnMin, voltLnMax, curLMin, curLMax, roomTempMin, roomTempMax, panelTempMin, panelTempMax, byWho, serialNumber];

            const queryUpdateRecti = " UPDATE im_rectifier " +
            " SET rectifier_name = $2, serial_number = $3, lat = $4, \"long\"= $5, desa_id = $6, kecamatan_id = $7, kota_id = $8, provinsi_id = $9, mqtt_topic = $10, last_update_who = $11, description = $12, last_update = now(), action = 'U'" +
            " WHERE rectifier_id = $1 ;";
            const queryUpdateThresh = " UPDATE mt_rectifier_threshold" +
            " SET voltage_load_min = $1, voltage_load_max = $2, current_load_min = $3, current_load_max = $4, voltage_batt_min = $5, voltage_batt_max = $6, current_batt_min = $7, current_batt_max = $8, voltage_ln_min = $9, voltage_ln_max = $10, current_l_min = $11, current_l_max = $12, room_temp_min = $13, room_temp_max = $14, panel_temp_min = $15, panel_temp_max = $16, last_update = now(), action = 'C', last_update_who = $17" +
            " WHERE serial_number = $18; ";

            await client.query(queryUpdateRecti, valueRectifier);
            await client.query(queryUpdateThresh, valueThreshold);

            await client.query('COMMIT');
        } catch (e) {
            await client.query('ROLLBACK');
            result(e, null);
            throw e
        } finally {
            client.release();
        }
        result(null, "success");
    })().catch(e => console.error(e.stack));
}

const deleteDevice = (request, result) => {
    const { id, serialNumber } = request.body;
    const byWho = request.cookies.username;


    (async () => {
        // note: we don't try/catch this because if connecting throws an exception
        // we don't need to dispose of the client (it will be undefined)
        const client = await pool.connect();
        try {
            await client.query('BEGIN');

            const valueRectifier = [id, byWho];

            const valueThreshold = [serialNumber, byWho];

            const queryUpdateRecti = " UPDATE im_rectifier " +
            " SET last_update_who = $2, last_update = now(), flag = 'N', action = 'D'" +
            " WHERE id = $1 ; ";

            const queryUpdateThresh = " UPDATE mt_rectifier_threshold " +
            " SET last_update_who = $2, last_update = now(), flag = 'N', action = 'D'" +
            " WHERE serial_number = $1 ; ";

            await client.query(queryUpdateRecti, valueRectifier);
            await client.query(queryUpdateThresh, valueThreshold);

            await client.query('COMMIT');
        } catch (e) {
            await client.query('ROLLBACK');
            result(e, null);
            throw e
        } finally {
            client.release();
        }
        result(null, "success");
    })().catch(e => console.error(e.stack));
}


module.exports = { getDevice, getDeviceById, getDeviceBySN, getDeviceByProvinsi, getDeviceByKota, insertDevice, updateDevice, deleteDevice};