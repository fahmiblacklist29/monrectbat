const { pool } = require("../config/database.js");


const getBattery = (result) => {
  pool.query("SELECT a.battery_id, a.battery_name, a.lat, a.long, a.desa_id, a.kecamatan_id, a.kota_id, a.provinsi_id, a.mqtt_topic, a.description, " +
      " b.desa_name, c.kecamatan_name, d.kota_name, e.provinsi_name" +
      " FROM im_battery a" +
      " LEFT JOIN im_desa b ON b.desa_id = a.desa_id" +
      " LEFT JOIN im_kecamatan c ON c.kecamatan_id = a.kecamatan_id" +
      " LEFT JOIN im_kota d ON d.kota_id = a.kota_id" +
      " LEFT JOIN im_provinsi e ON e.provinsi_id = a.provinsi_id" +
      " WHERE a.flag = 'Y' ORDER BY a.battery_id ASC", (error, results) => {
          if (error) {
              result(error, null);
          }
          if (results.rowCount > 0) {
              result(null,
                  results.rows.sort((a, b) => {
                      return a.kota_id.localeCompare(b.kota_id);
                  }));
          } else {
              result(null, JSON.parse("[]"));
          }

      })
}

const getBatteryById = (request, result) => {
  const id = request.params.id;

  pool.query("SELECT a.battery_id, a.battery_name, a.lat, a.long, a.desa_id, a.kecamatan_id, a.kota_id, a.provinsi_id, a.mqtt_topic, a.description, " +
      " b.desa_name, c.kecamatan_name, d.kota_name, e.provinsi_name" +
      " FROM im_battery a" +
      " LEFT JOIN im_desa b ON b.desa_id = a.desa_id" +
      " LEFT JOIN im_kecamatan c ON c.kecamatan_id = a.kecamatan_id" +
      " LEFT JOIN im_kota d ON d.kota_id = a.kota_id" +
      " LEFT JOIN im_provinsi e ON e.provinsi_id = a.provinsi_id" +
      " WHERE a.flag = 'Y' AND a.battery_id = $1", [id], (error, results) => {
          if (error) {
              result(error, null);
          }
          result(null, results.rows);
      })
}

const getBatteryBySN = (request, result) => {
  const id = request.params.id;

  pool.query("SELECT a.battery_id, a.battery_name, a.lat, a.long, a.desa_id, a.kecamatan_id, a.kota_id, a.provinsi_id, a.mqtt_topic, a.description, " +
      " b.desa_name, c.kecamatan_name, d.kota_name, e.provinsi_name" +
      " FROM im_battery a" +
      " LEFT JOIN im_desa b ON b.desa_id = a.desa_id" +
      " LEFT JOIN im_kecamatan c ON c.kecamatan_id = a.kecamatan_id" +
      " LEFT JOIN im_kota d ON d.kota_id = a.kota_id" +
      " LEFT JOIN im_provinsi e ON e.provinsi_id = a.provinsi_id" +
      " WHERE a.flag = 'Y' AND a.serial_number = $1", [id], (error, results) => {
          if (error) {
              result(error, null);
          }
          result(null, results.rows);
      })
}

const getBatteryByProvinsi = (request, result) => {
  const id = request.params.id;

  pool.query("SELECT a.battery_id, a.battery_name, a.lat, a.long, a.desa_id, a.kecamatan_id, a.kota_id, a.provinsi_id, a.mqtt_topic, a.description, " +
      " b.desa_name, c.kecamatan_name, d.kota_name, e.provinsi_name" +
      " FROM im_battery a" +
      " LEFT JOIN im_desa b ON b.desa_id = a.desa_id" +
      " LEFT JOIN im_kecamatan c ON c.kecamatan_id = a.kecamatan_id" +
      " LEFT JOIN im_kota d ON d.kota_id = a.kota_id" +
      " LEFT JOIN im_provinsi e ON e.provinsi_id = a.provinsi_id" +
      " WHERE a.flag = 'Y' AND a.provinsi_id = $1", [id], (error, results) => {
          if (error) {
              result(error, null);
          }
          result(null, results.rows);
      })
}

const getBatteryByKota = (request, result) => {
  const id = request.params.id;

  pool.query("SELECT a.battery_id, a.battery_name, a.lat, a.long, a.desa_id, a.kecamatan_id, a.kota_id, a.provinsi_id, a.mqtt_topic, a.description, " +
      " b.desa_name, c.kecamatan_name, d.kota_name, e.provinsi_name" +
      " FROM im_battery a" +
      " LEFT JOIN im_desa b ON b.desa_id = a.desa_id" +
      " LEFT JOIN im_kecamatan c ON c.kecamatan_id = a.kecamatan_id" +
      " LEFT JOIN im_kota d ON d.kota_id = a.kota_id" +
      " LEFT JOIN im_provinsi e ON e.provinsi_id = a.provinsi_id" +
      " WHERE a.flag = 'Y' AND a.kota_id = $1", [id], (error, results) => {
          if (error) {
              result(error, null);
          }
          result(null, results.rows);
      })
}

const insertBattery = (request, result) => {
  const { name, lat, lon, desa, kecamatan, kota, provinsi, serialNumber, description } = request.body;
  const byWho = request.cookies.username;


  (async () => {
      // note: we don't try/catch this because if connecting throws an exception
      // we don't need to dispose of the client (it will be undefined)
      const client = await pool.connect();
      try {
          await client.query('BEGIN');

          const mqttTopic = "monRectiData/Battery/" + serialNumber;

          const querySeqbattery = "SELECT nextval('seq_battery');"
          const seqRecti = await client.query(querySeqbattery);

          //const querySeqThresh = "SELECT nextval('seq_battery_monitoring');"
          //const seqThresh = await client.query(querySeqThresh);

          const valuebattery = [seqRecti.rows[0].nextval, name, lat, lon, desa, kecamatan, kota, provinsi, mqttTopic, serialNumber, byWho, description];
          //const valueThreshold = [seqThresh.rows[0].nextval, serialNumber ];

          const queryInsertRecti = " INSERT INTO im_battery(" +
              ' battery_id, battery_name, lat, "long", desa_id, kecamatan_id, kota_id, provinsi_id, mqtt_topic, serial_number, created_who, description, created_date, last_update_who, last_update, flag, action)' +
              " VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11,  $12, now(), $11, now(), 'Y', 'C');";
          // const queryInsertThresh = " INSERT INTO mt_cell_monitoring(" +
          //     " id, sn_battery,, created_date, last_update, flag, action) " +
          //     " VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, $13, $14, $15, $16, $17, $18, now(), now(), 'Y', 'C');";

          await client.query(queryInsertRecti, valuebattery);
          //await client.query(queryInsertThresh, valueThreshold);

          await client.query('COMMIT');
      } catch (e) {
          await client.query('ROLLBACK');
          result(e, null);
          throw e
      } finally {
          client.release()
      }
      result(null, "success");
  })().catch(e => console.error(e.stack));
}



module.exports = { getBattery, getBatteryById, getBatteryBySN, getBatteryByProvinsi, getBatteryByKota, insertBattery};