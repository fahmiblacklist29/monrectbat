const bingmaps = require("bingmaps");

const getMap = (location) =>{
    const map = new Microsoft.Maps.Map('#dashboard_map', {
        credential : 'Av-n2kHUF9cJrm5yXImVswuOxWkR-59GMrHmL2Mf5mx0t41OfhVmN7yAQN8RIF4M'
    });

    const displayMap = map.setView({
        center: new Microsoft.Maps.Location(location.long, location.lat),
        mapTypeId: Microsoft.Maps.MapTypeId.canvasDark,
        zoom: 10
    });
    // console.log(displayMap);
}

const generatePin = (pinCollection)=>{
    pinCollection.forEach(locPin => {
        var pin = new Microsoft.Maps.Pushpin(
            new Microsoft.Maps.Location(locPin.long,locPin.lat), {
            icon: 'img/trf_grey.png',
        });
        //Microsoft.Maps.Events.addHandler(pin, 'click', function () { alert("pin clicked") });
        
        map.entities.push(pin);
    });
}

module.exports = {getMap, generatePin};